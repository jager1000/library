﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Cloud.Analytics;

public class UnityAnalyticsIntegration : MonoBehaviour {

    int notesCollected = 4;
    // Use this for initialization
    void Start () {
        
        const string projectId = "5b4e381d-2b72-4fda-b040-6f1b9e972703";
        UnityAnalytics.StartSDK (projectId);    
    }
}