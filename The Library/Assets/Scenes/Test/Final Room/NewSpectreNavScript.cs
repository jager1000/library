﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
using UnityStandardAssets.Characters.FirstPerson;

// This script controlls the Spectre's Player detection and movement over the navMesh
public class NewSpectreNavScript : MonoBehaviour {

	public GameObject playerObject;
	public GameObject podiumPos;
	private SphereCollider detectZone;
	public float detectStillDistance;
	public float detectCrouchDistance;
	public float detectWalkDistance;
	public float detectRunDistance;
	public float detectJumpDistance;
	public float detectHoldBreathDistance;
	public float detectReleaseBreathDistance;
	public float followSpeed;
	public float wanderSpeed;
	public float speed = 0.5f;
	public float blindness = 0.9f;
	public float canSee = 0.5f;
	private GameObject target;
	private bool followPlayer = false;
	private string playerState;
	NavMeshAgent spectreNav;
	private float currentSpeed;
	private BlurOptimized effects;

	//new nav code
	public GameObject[] set1WayPoint;
	public GameObject[] set2WayPoint;
	private int index = 0;
	public GameObject TransPoint1;
	public GameObject TransPoint2;
	public GameObject TransTriggerPoint1;
	public GameObject TransTriggerPoint2;
	public GameObject spectreSpawnPoint;
	private string navMode = "Circle";
	//
	private Vector3 currentPos;
	private int quickCountDown = 200;
    public GameObject passout;
	
	//sounds
	public AudioSource source1;
	public AudioSource source2;
	
	// Use this for initialization
	void Start () {

		//attach librarian to nav mesh and set up detection
		spectreNav  = GetComponent<NavMeshAgent>();			
		detectZone = transform.GetComponent<SphereCollider> ();

		//find waypoints and assing to their arrays
		set1WayPoint = GameObject.FindGameObjectsWithTag("Waypoints");
		set2WayPoint = GameObject.FindGameObjectsWithTag ("WaypointSet2");

		this.transform.position = spectreSpawnPoint.transform.position;

		//start travelling on points
		if (set1WayPoint.Length != 0) {
			target = set1WayPoint [index];
			spectreNav.SetDestination (target.transform.position);
			effects = playerObject.GetComponentInChildren<BlurOptimized> ();
		} else {
			print ("Spectre navigation script needs waypoints, set waypoint size greater than 0");
		} //end else/if

	} // end Start

    IEnumerator Wait() {
        yield return new WaitForSeconds(1);
        playerObject.transform.position = podiumPos.transform.position;
        passout.GetComponent<ScreenFader>().fadeIn = true;
    }

    // Update is called once per frame
	void Update () {
		//take the player's movement state and change detectzone collider's size
		playerState = playerObject.GetComponent<FirstPersonController>().playerMovementState;
		currentSpeed = GetComponent<SpectreAnimation> ().spectreSpeed;
		if (playerState == "isStill") {
			detectZone.radius = detectStillDistance;}
		if (playerState == "isHoldingBreath") {
			detectZone.radius = detectHoldBreathDistance;}
		if (playerState == "isJumping") {
			detectZone.radius = detectJumpDistance;}
		if (playerState == "isRunning") {
			detectZone.radius = detectRunDistance;}
		if (playerState == "isCrouching") {
			detectZone.radius = detectCrouchDistance;}
		if (playerState == "isWalking") {
			detectZone.radius = detectWalkDistance;}

		//player reset to podium when close
		if(Vector3.Distance(transform.position, playerObject.transform.position) <= 1.5){
            passout.GetComponent<ScreenFader>().fadeIn = false;
            StartCoroutine(Wait());
                    
            //reset triggers for maze and reset navMode
			navMode = "Circle";
			index = 0;
			TransTriggerPoint1.GetComponent<TriggerScript>().triggerSpent = false;
			TransTriggerPoint1.GetComponent<TriggerScript>().hitTrigger = false;
			TransTriggerPoint2.GetComponent<TriggerScript>().triggerSpent = false;
			TransTriggerPoint2.GetComponent<TriggerScript>().hitTrigger = false;
			this.transform.position = spectreSpawnPoint.transform.position;
     
        } //end if close to player

		//Detect triggers on transition waypoint and change navMode if triggered (be sure to reset on player death)
		if (TransTriggerPoint1.GetComponent<TriggerScript>().hitTrigger == true) {
			navMode = "TransitionToPace";
			target = TransPoint1;
			spectreNav.speed = followSpeed;
			spectreNav.SetDestination(target.transform.position);
			TransTriggerPoint1.GetComponent<TriggerScript>().hitTrigger = false;
		}
		if (TransTriggerPoint2.GetComponent<TriggerScript>().hitTrigger == true) {
			navMode = "TransitionToChase";
			target = TransPoint2;
			spectreNav.speed = followSpeed;
			spectreNav.SetDestination(target.transform.position);
			TransTriggerPoint2.GetComponent<TriggerScript>().hitTrigger = false;
		}

		if (followPlayer == true) {
			quickCountDown --;
			if (quickCountDown <= 10){
				followPlayer = false;
				quickCountDown = 200;
			}

		}
		//If the player leaves the collider, Speed will slow to wander speed and lib moves to nav point
		if (followPlayer == false) {

			if (spectreNav.speed != wanderSpeed && navMode != "Chase" && navMode != "TransitionToChase" && navMode != "TransitionToPace"){
				spectreNav.speed = wanderSpeed;
			} //end if speed

			// if reaches target or cant reach target
			if (Vector3.Distance(transform.position, target.transform.position) <= 1 || currentSpeed <= 0.01){
				index++;

				if (navMode == "Circle"){
					if (index >= set1WayPoint.Length){
						index = 0;
					} //end if index
					target = set1WayPoint[index];
				} //end if circle
				if (navMode == "TransitionToPace"){
					//target = TransPoint1;
					navMode = "Pace";
				} //end if transition1
				if (navMode == "Pace"){
					if (index >= set2WayPoint.Length){
						index = 0;
					} //end if index
					target = set2WayPoint[index];
				} //end if pace
				if (navMode == "TransitionToChase"){
					//target = TransPoint2;
					navMode = "Chase";
				} //end if transition2
				if (navMode == "Chase"){
					if (spectreNav.speed != followSpeed){
						spectreNav.speed = followSpeed;
					} //end if speed
					target = playerObject;
				} //end if chase
			
				//target = wayPoint[index%wayPoint.Length]; //cut from this version
				spectreNav.SetDestination(target.transform.position);

			} //end if reach target

		} //end if not follow

	} //end Update
	
	// If the player is within the collider, the librarian will take players position and speeds to alerted pace
	void OnTriggerEnter(Collider detectedObject) {
		if (detectedObject.tag == ("Player")){
			followPlayer = true;
			spectreNav.speed = followSpeed;
			//effects.enabled = true;
			// play alearted sound
			if (!source1.isPlaying && !source2.isPlaying){
				//source.pitch = Random.Range (0.95f, 1.05f);
				source2.Play();
			}

		} //end if player

	} //end OnTriggerEnter
	
	void OnTriggerStay(Collider detectedObject){
		if (detectedObject.tag == ("Player")) {
			spectreNav.SetDestination (playerObject.transform.position);
			quickCountDown = 200;
		} //end if player

	}//end OnTriggerStay

	void OnTriggerExit(Collider detectedObject){
		if (detectedObject.tag == ("Player")) {
			// play dissapoint sound and go back to navpoints
            if(!source1.isPlaying){
				source1.PlayDelayed(1.5f);
            }	
			followPlayer = false;
			spectreNav.speed = wanderSpeed;
		} //end if player

	}//end OnTriggerExit

} //end class
