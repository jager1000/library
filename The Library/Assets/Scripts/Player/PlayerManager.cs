﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {

    public static bool isHidden = false;
    public GameObject breathRadius;
    public GameObject hidingRadius;
    private float timePressed = 0.0f;
    private float breathHoldTimeStart;
	private float breathHoldTimeEnd;

	void Start () {
        breathHoldTimeStart = 0;
        breathRadius.SetActive(true);
        hidingRadius.SetActive(false);
	}
	
	void Update () {
        breathHoldTimeStart = Time.time;
		breathHoldTimeEnd = breathHoldTimeStart + 10.0f;
		breathHoldTimeStart += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Q)){
            breathRadius.SetActive(false);
            hidingRadius.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Q) && breathHoldTimeStart >= Time.deltaTime){
            breathRadius.SetActive(false);
            hidingRadius.SetActive(true);
        }
		if (Input.GetKeyUp(KeyCode.Q) || breathHoldTimeStart >= breathHoldTimeEnd){
            print(timePressed);
            breathRadius.SetActive(true);
            hidingRadius.SetActive(false);
        }
	}
}
