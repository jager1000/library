﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadLevel : MonoBehaviour {

    public GameObject delete1;
    public GameObject delete2;
    public GameObject delete3;
    public Text loadingBar;
    IEnumerator Start() {
        AsyncOperation async = Application.LoadLevelAdditiveAsync(1);
        Destroy(delete1);
        Destroy(delete2);
        Destroy(delete3);
        while(!async.isDone){
            loadingBar.enabled = true;
            yield return async;

        }

    }
}