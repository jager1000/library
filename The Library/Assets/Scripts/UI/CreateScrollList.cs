﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
/// <summary>
/// Custom attributes for each item in the scroll list
/// </summary>
public class Item{
    public string name;
    public Button.ButtonClickedEvent thingToDo;
}

public class CreateScrollList : MonoBehaviour {
    public List<Item> itemList;
    public Transform contentPanel;
    public GameObject sampleButton;
    public GameObject journalHolder;
    public GameObject pauseMenu;
    private bool paused = false;
    public Camera camera;
    public Image[] diary;
    public Text[] journals = new Text[4];

    public Button resumeText;
    public Button restartText;
    public Button quitText;

    public GameObject chest;
    
    void Start () {
        journalHolder.SetActive(false);
        pauseMenu.SetActive(false);
	}

    void Update () {
        if(Input.GetButtonDown("Journal") && journalHolder.activeInHierarchy == false){
            pauseMenu.SetActive(false);
            //PopulateList();
            for(int i = 0; i < 4; i++){
                if(Inventory.notes[i] == true){
                journals[i].enabled = true;
                }
            }
            Cursor.lockState = CursorLockMode.None;
            journalHolder.SetActive(true);
            Time.timeScale = 0;
            paused = !paused;
        }else
        if(Input.GetButtonDown("Journal") && journalHolder.activeInHierarchy == true){
            for(int i = 0; i < diary.Length; i++){
                diary[i].enabled = false;
            }
            journalHolder.SetActive(false);
            pauseMenu.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
            paused = !paused;
        }
        if(Input.GetKeyUp(KeyCode.Escape) && journalHolder.activeInHierarchy == true){
            for(int i = 0; i < diary.Length; i++){
                diary[i].enabled = false;
            }
            pauseMenu.SetActive(false);
            journalHolder.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
        }else
        if(Input.GetKeyUp(KeyCode.Escape) && journalHolder.activeInHierarchy == false && pauseMenu.activeInHierarchy == false){
            chest.SetActive(false);
//            chest.GetComponent<Collider>().isTrigger = false;
//            chest.GetComponent<Collider>().enabled = false;
            journalHolder.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            pauseMenu.SetActive(true);
            Time.timeScale = 0;
            paused = !paused;
        }else
        if(Input.GetKeyUp(KeyCode.Escape) && pauseMenu.activeInHierarchy == true){
            pauseMenu.SetActive(false);
            chest.SetActive(true);
            journalHolder.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
            paused = !paused;
        }
    }

    public void resumePress(){
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        pauseMenu.SetActive(false);
        chest.SetActive(true);
//        chest.GetComponent<Collider>().isTrigger = true;
//        chest.GetComponent<Collider>().enabled = true;
    }
    
    public void restartPress(){
        Application.LoadLevel(Application.loadedLevel);
        Time.timeScale = 1;
    }
    
    public void quitPress(){
        //Application.Quit ();
        System.Diagnostics.Process.GetCurrentProcess().Kill();
    }
    

    void PopulateList(){
//        foreach(var items in itemList){
//                if(Inventory.notes[itemList.IndexOf(items)] == true){
//                    GameObject newButton = Instantiate(sampleButton) as GameObject;
//                    SampleButton button = newButton.GetComponent<SampleButton>();
//                    button.name.text = items.name;
//                    button.button.onClick = items.thingToDo;
//                    newButton.transform.SetParent(contentPanel,false);
//                Inventory.notes[itemList.IndexOf(items)] = false;
//                }
//            }
        }
    }
