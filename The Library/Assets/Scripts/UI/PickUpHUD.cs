﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PickUpHUD : MonoBehaviour {

    public Image key;
    public Image book;
    public Image log;
	// Use this for initialization
	void Start () {
        key.enabled = false;
        book.enabled = false;
        log.enabled = false;
	}
	
	void Update () {
        if(Inventory.keys[1] == true){
            key.enabled = true;
        }
        if(Inventory.objectives[1] == true){
            book.enabled = true;
        }
        if(Inventory.objectives[0] == true){
            log.enabled = true;
        }
	}
}
