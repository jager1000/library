﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class onMouse : MonoBehaviour {

	public RawImage blood;
	
	// Use this for initialization
	void Start () {
		blood.enabled = false;
	}

	void Update(){
		OnMouseOver ();
		OnMouseExit ();
	}

	void OnMouseOver(){
		blood.enabled = true;
		//Debug.Log ("gets here");
	}

	void OnMouseExit(){
		blood.enabled = false;
	}
}
