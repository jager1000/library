﻿using UnityEngine;
using System;
using System.Collections;

public class SoundPlaceHolder : MonoBehaviour {

    public  AudioClip[] CautiousScared;
    public  AudioClip[] BreatheIn;
    public  AudioClip[] BreatheOut;
    public  AudioClip[] BreathStruggle;
    public  AudioClip[] BreathRelief;
    public  AudioClip[] BookCarpet;
    public  AudioClip[] BookStone;
    public  AudioClip[] BookWood;
    public  AudioClip[] CreakyFloor;
    public  AudioClip[] CreakyDoor;
    public  AudioClip[] DoorKnocking;
    public  AudioClip[] Unlock;
    public  AudioClip[] Open;
    public  AudioClip[] Close;
    public  AudioClip[] Handle;
    public  AudioClip[] TurnPage;
    public  AudioClip[] Attenton;
    public  AudioClip[] Passive;
    public  AudioClip[] Searching;
    public  AudioClip[] Spotted;
    public  AudioClip OpenSecretDoor;
    public  AudioClip FumblingUnlock;
    public  AudioClip InsertKey;
    public  AudioClip PanicUnlock1;
    public  AudioClip PanicUnlock2;
    public  AudioClip LibrarianProximity1;
    public  AudioClip LibrarianProximity2;
    public  AudioClip PickupKey;
    public  AudioClip Scream;
}
