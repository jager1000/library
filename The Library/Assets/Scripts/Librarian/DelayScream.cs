﻿using UnityEngine;
using System.Collections;

public class DelayScream : MonoBehaviour {


	private AudioSource source;

	void Start () {
		source = GetComponent<AudioSource> ();
		source.PlayDelayed (0.3f);
	}
}
