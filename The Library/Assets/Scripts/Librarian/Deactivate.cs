﻿using UnityEngine;
using System.Collections;

public class Deactivate : MonoBehaviour {

	public float timeToDestroy = 10f;

	void Start(){
		Destroy (gameObject, timeToDestroy);
	}
}
