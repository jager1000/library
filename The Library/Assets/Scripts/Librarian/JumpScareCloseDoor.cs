﻿using UnityEngine;
using System.Collections;

public class JumpScareCloseDoor : MonoBehaviour {

	public DoorScript door;
	public TheRealJumpScare trigger;

	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Player")) {
			if (door.open){
				door.ChangeDoorState();
			}
			trigger.otherTriggered = true;
			Destroy (gameObject);
		}
	}
}
