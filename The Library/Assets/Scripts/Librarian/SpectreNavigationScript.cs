﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using UnityStandardAssets.Characters.FirstPerson;

public class SpectreNavigationScript : MonoBehaviour {
// This script controlls the Spectre's Player detection and movement over the navMesh
	//public Transform target;		//just a nav test target
	public GameObject playerObject;
    public GameObject podiumPos;
	private SphereCollider detectZone;
	public float detectStillDistance;
	public float detectCrouchDistance;
	public float detectWalkDistance;
	public float detectRunDistance;
	public float detectJumpDistance;
	public float detectHoldBreathDistance;
	public float detectReleaseBreathDistance;
	public float followSpeed;
	public float wanderSpeed;
    public float speed = 0.5f;
    public float blindness = 0.9f;
    public float canSee = 0.5f;
	private GameObject target;
	private bool followPlayer = false;
	private string playerState;
	NavMeshAgent spectreNav;
	private float currentSpeed;
    private BlurOptimized effects;

	//copy from other
	public GameObject[] wayPoint;
	private Vector3 currentPos;
	private int index = 0;

	//sounds
	private AudioSource source;
	public AudioClip[] alert;
	public AudioClip[] passive;
	public TriggerTensionSynth triggerSpotted;
	
	// Use this for initialization
	void Start () {
		spectreNav  = GetComponent<NavMeshAgent>();			//attach librarian to nav mesh
		detectZone = transform.GetComponent<SphereCollider> ();
		wayPoint = GameObject.FindGameObjectsWithTag("Waypoints");
		if (wayPoint.Length != 0) {
			target = wayPoint [index % wayPoint.Length];		//start travelling on points
			spectreNav.SetDestination (target.transform.position);
			effects = playerObject.GetComponentInChildren<BlurOptimized> ();
		} else {
		print ("Spectre navigation script needs waypoints, set waypoint size greater than 0");}

		source = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		// This script will take the player's movement state and change detectzone collider's size
		playerState = playerObject.GetComponent<FirstPersonController>().playerMovementState;
		currentSpeed = GetComponent<SpectreAnimation> ().spectreSpeed;

        if(Vector3.Distance(transform.position, playerObject.transform.position) <= 10 && effects.blurSize <= 5){
            effects.blurSize += effects.blurSize * Time.deltaTime * blindness;
        }else if(Vector3.Distance(transform.position, playerObject.transform.position) > 10 && effects.blurSize >= 0){
            effects.blurSize -= effects.blurSize * Time.deltaTime * canSee;
        }

        if(Vector3.Distance(transform.position, playerObject.transform.position) <= 2){
            playerObject.transform.position = podiumPos.transform.position;
        }


		if (playerState == "isStill") {
			detectZone.radius = detectStillDistance;}
		if (playerState == "isHoldingBreath") {
			detectZone.radius = detectHoldBreathDistance;}
		if (playerState == "isJumping") {
			detectZone.radius = detectJumpDistance;}
		if (playerState == "isRunning") {
			detectZone.radius = detectRunDistance;}
		if (playerState == "isCrouching") {
			detectZone.radius = detectCrouchDistance;}
		if (playerState == "isWalking") {
			detectZone.radius = detectWalkDistance;}


		// If the player leaves the collider, Speed will slow to wander speed
		// when spectre reaches target position it takes random nav point as new target
		// figure out how to make this delay 1/2 secs so the spectre contiunes to the last known player pos for a while before giving up
		if (followPlayer == false) {
			if (Vector3.Distance(transform.position, target.transform.position) <= 3 || currentSpeed == 0){
				index++;
				target = wayPoint[index%wayPoint.Length];
				spectreNav.SetDestination(target.transform.position);
			}
		}
	}

	// If the player is within the collider, the librarian will take players position and speeds to alerted pace
	void OnTriggerEnter(Collider detectedObject)
	{
		if (detectedObject.tag == ("Player")){
			followPlayer = true;
			spectreNav.speed = followSpeed;
            effects.enabled = true;
		// play alearted sound
			if (!source.isPlaying){
				//source.pitch = Random.Range (0.95f, 1.05f);
				source.PlayOneShot(alert[Random.Range (0,alert.Length)], 0.45f);
				triggerSpotted.PlaySpotted();
			}
		}
	}
	void OnTriggerStay(Collider detectedObject)
	{
		if (detectedObject.tag == ("Player")) {
			spectreNav.SetDestination (playerObject.transform.position);
		}
	}
	void OnTriggerExit(Collider detectedObject)
	{
		if (detectedObject.tag == ("Player")) {
			// play dissapoint sound and go back to navpoints
			source.PlayOneShot(passive[Random.Range (0,passive.Length)]);
			followPlayer = false;
			spectreNav.speed = wanderSpeed;
		}
	}
}
