﻿using UnityEngine;
using System.Collections;

public class FirstSeenTrigger : MonoBehaviour {

	public GameObject librarian;
	public AudioSource source;

	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Player")) {
			librarian.SetActive(true);
			source.Play();
			Destroy (gameObject);
		}
	}

}
