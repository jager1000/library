﻿using UnityEngine;
using System.Collections;

public class HallwayScare : MonoBehaviour {


	public bool triggered = false;
	private bool done = false;
	public AudioSource source;
	public AudioClip theScare;
 
	// Update is called once per frame
	void Update () {

		if (triggered) {
			transform.Translate (Vector3.forward * Time.deltaTime * 7.5f);
			if (!done) {
				source.PlayOneShot (theScare, 1f);
			}
			done = true;
			Destroy (gameObject, 12f);
		}
	}

}
