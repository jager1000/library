﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class NearWindow : MonoBehaviour {

	private AudioSource source;
	public AudioMixerSnapshot inside;
	private float m_WindFadeIn = 3f;

	private bool windowClosed = false;

	void Start(){
		source = GetComponent<AudioSource> ();
	}

	public void NowInside(){
		windowClosed = true;
		inside.TransitionTo(2f);
	}

	void Update(){
		if (windowClosed && source.spatialBlend < 1f) {
			source.spatialBlend += 0.25f * Time.deltaTime;
		}
	}

}


