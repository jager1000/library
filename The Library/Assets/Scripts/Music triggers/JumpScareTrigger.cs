﻿using UnityEngine;
using System.Collections;

public class JumpScareTrigger : MonoBehaviour {

	public AudioClip clockStrike;
	private bool oneShot = false;
	private AudioSource source;

	void Awake(){
		source = GetComponent<AudioSource>();
	}

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Player") && oneShot == false){
			source.PlayOneShot(clockStrike, 1F);
			oneShot = true;
		}
	}
}
