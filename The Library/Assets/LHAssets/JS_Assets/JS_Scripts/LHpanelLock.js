/*This script handles a panel lock, which requires the activator to move plugs into the correct slots in order to unlock it. 
You must also make sure no wires are intersecting, and there is a limited number of moves activator*/

#pragma strict
//#pragma downcast

private var thisTransform:Transform;

//The camera of this lock
var cameraObject:Camera;

//List of the panel plugs, and which plugs they connect to with wires
var plugs:Plugs[];

class Plugs 
{
    var plug:Transform;
    var connectsTo:Transform;
}

//The currently selected slot.
internal var currentSlot:Transform;

//The currently selected plug.
internal var currentPlug:Transform;

//Colors of the correct plu icon
var colorSlotCorrect:Color;
var colorSlotWrong:Color;

//Did we succeed or fail in unlocking this lock?
private var success:boolean = false;
private var failure:boolean = false;

//Are we moving a plug now?
private var isMoving:boolean = false;

//How many times we can move the plugs around. If you lift a plug and then plug it back into the same slot, it doesn't count as a move
var movesLeft:int = 10;

//The name of the tool in the player's inventory which is required to unlock this lock ( lockpicks, bobbyplugs, safe cracker tools, etc )
var requiredTool:String = "";

//The index of the tool in the player's inventory
static var requiredToolIndex:int;

//The container of this lock ( A door, a safe, etc )
internal var lockParent:Transform;

//The activator object
internal var activator:GameObject;

//Various sounds
var soundPlugOut:AudioClip;
var soundPlugIn:AudioClip;
var soundUnlock:AudioClip;
var soundFail:AudioClip;

//GUI for button graphics
var GUISkin:GUISkin;

//The position and size of the "abort" button
var abortRect:Rect = Rect( 0, 0, 100, 50);
var abortText:String = "Abort";

//The position and size of the "moves left" display
var movesLeftRect:Rect = Rect( 0, 50, 100, 50);
var movesLeftText:String = "Moves left: ";

//The description for how to play this lock
var description:String = "Click on a plug to pick it up, then click on an empty slot to plug it in.\nPut all the plugs in the correct slots and don't let the wires intersect.";
var descriptionMobile:String = "Tap on a plug to pick it up, then tap on an empty slot to plug it in.\nPut all the plugs in the correct slots and don't let the wires intersect.";

//A check for control types ( default, mobile, etc )
private var controlsType:String = "";

//Should we lock the mouse pointer on Exit?
var lockCursorOnExit:boolean = false;

function Start()
{
	//Check if we are running on a mobile and set the controls accordingly
	#if UNITY_IPHONE
    	controlsType = "iphone";
    	print("iphone");
  	#endif
  	
  	#if UNITY_ANDROID
    	controlsType = "android";
    	print("android");
  	#endif
  	
	//Caching the transform for quicker access
	thisTransform = this.transform;
	
	//Deactivate the activator and container objects so they don't interfere with the lockpicking gameplay
	if ( activator )    activator.SetActive(false);
	
	//Update the positions of the wires and their colliders
	UpdateWires();
	
	//Check if we met all the success goals; all plugs in correct slots, and no wires intersecting
	CheckSuccess();
}

function Update() 
{
	//Show the cursor and allow it to move while we interact with the lock 
	Cursor.lockState = CursorLockMode.None;
	Cursor.visible = true;
	
	if ( failure == false && success == false )
	{
		//If we are not moving a plug, look for a plug to select
		if ( isMoving == false )
		{
			var hit:RaycastHit;
		    var ray:Ray = cameraObject.ScreenPointToRay(Input.mousePosition);
			
			//If on mobile, check from camera object to touch position
			if ( (controlsType == "android" || controlsType == "iphone") && Input.touches.Length > 0 )    ray = cameraObject.ScreenPointToRay(Input.touches[0].position);
			
			if ( Physics.Raycast( ray, hit, 100) )
		    {
				//Allow checks only with a slot
			    if ( hit.collider.tag == "LHslot" )
			    {
					//Set the current slot
					currentSlot = hit.collider.transform;
					
					//If we are on mobile, Check for a touch to move the plug, otherwise check for a click
					if ( (controlsType == "android" || controlsType == "iphone") && Input.touches.Length > 0 )
					{
						if ( Input.touches[0].phase == TouchPhase.Began )
						{
							if ( currentPlug == null && currentSlot.Find("Plug") )    PlugOut();
							else if ( currentPlug && currentSlot.Find("Plug") == null )    PlugIn(currentSlot);
						}
					}
					else if ( Input.GetButtonDown("Fire1") )
					{
						if ( currentPlug == null && currentSlot.Find("Plug") )    PlugOut();
						else if ( currentPlug && currentSlot.Find("Plug") == null )    PlugIn(currentSlot);
					}
				}
					
			}
		}
	}
}

//This function updates the positions of the wires and their colliders
function UpdateWires()
{
	//Go through all the plugs, update the positions of the wires and their colliders to strech correctly between the plugs
	for ( var plug in plugs )
	{
		//Strech the wire between two plugs
		plug.plug.Find("Wire").GetComponent(LineRenderer).SetPosition(0, plug.plug.Find("Wire").position);
		plug.plug.Find("Wire").GetComponent(LineRenderer).SetPosition(1, plug.connectsTo.Find("Wire").position);
		
		//Rotate the wire to look at the next plug
		plug.plug.Find("Wire").LookAt(plug.connectsTo.Find("Wire"));
		
		//Strech the collider of the wire between two plugs
		plug.plug.Find("Wire").GetComponent(BoxCollider).center.z = Vector3.Distance(plug.plug.Find("Wire").position, plug.connectsTo.Find("Wire").position) * 0.5;
		plug.plug.Find("Wire").GetComponent(BoxCollider).size.z = Vector3.Distance(plug.plug.Find("Wire").position, plug.connectsTo.Find("Wire").position) * 0.8;
	}
}

//This function checks if we met all the success goals: all plugs in correct slots, and no wires intersecting
function CheckSuccess()
{
	//Reset the correct slots counter
	var correctSlots:int = 0;
	
	//Go through all the plugs
	for ( var plug in plugs )
	{
		//If this plug is in a correct slot, proceed to the next check
		if ( plug.plug.parent.Find("CorrectSlot") )    
		{
			//If the wire of this plug is not intersecting with other wires, increase the correctSlots count
			if ( plug.plug.Find("Wire").GetComponent(LHpanelWire).isIntersecting == false )    correctSlots++;
			
			//Change the color of the slot
			plug.plug.parent.Find("CorrectSlot").GetComponent.<Renderer>().material.color = colorSlotCorrect;
		}
	}
	
	//If the number of correct slots is the same as the number of all the slots, then SUCCESS
	if ( correctSlots == plugs.Length )
	{
		success = true;
		
		//Unlock this lock
		Unlock();
	}
	else if ( movesLeft <= 0 )
	{
		//If no moves are left, we fail
		Fail();
	}
}

//This function moves the plug out of the slot
function PlugOut()
{
	GetComponent.<AudioSource>().PlayOneShot(soundPlugOut);
	
	//Set current plug
	currentPlug = currentSlot.Find("Plug");
	
	//Reset the color of the correct slot to it's wrong state ( not plugged in )
	if ( currentPlug.parent.Find("CorrectSlot") )    currentPlug.parent.Find("CorrectSlot").GetComponent.<Renderer>().material.color = colorSlotWrong;
			
	//Move the plug out of the slot																
	MoveTo(currentPlug, Vector3(currentPlug.position.x,currentPlug.position.y,currentPlug.position.z + 0.2), 0.2);
	
	//Remove the plug from the hierarchy so we can place it in another slot
	currentPlug.parent = null;
}

//This function moves the plug to the selected slot, and then plugs it in
function PlugIn( plugSlot:Transform )
{
	//If we selected a slot which is not the same as the slot of the selected plug, then we move the plug to the new slot
	if ( currentPlug.position.x != plugSlot.position.x || currentPlug.position.y != plugSlot.position.y )
	{
		//Reduce from the moves left. If this reaches 0, we fail
		movesLeft--;
		
		//Move the plug to the new slot position
		MoveTo(currentPlug, Vector3(plugSlot.position.x,plugSlot.position.y,currentPlug.position.z), 0.2);
		
		yield WaitForSeconds(0.3);
	}
	
	//Move the plug into the slot
	MoveTo(currentPlug, plugSlot.position, 0.2);
	
	//Place the plug in the hierarchy of this slot
	currentPlug.parent = plugSlot;
	
	//Clear the current plug
	currentPlug = null;
	
	GetComponent.<AudioSource>().PlayOneShot(soundPlugIn);
	
	yield WaitForSeconds(0.2);
	
	//Check if we meet the goals
	CheckSuccess();
}

//This function moves an object to another point, with a delay
function MoveTo(movedObject:Transform, targetPosition:Vector3, moveTime:float)
{
	//We are now moving
	isMoving = true;
	
	//Used to help us calculate the speed of movement
	var timeLeft:float = moveTime;
	
	while ( timeLeft > 0 )
	{
		timeLeft -= Time.deltaTime;
		
		yield WaitForSeconds(Time.deltaTime);
		
		//Move the object in increments from 0 to moveTime (0 is where the object started, 0.5 is halfway through, and 1 is the end of the path)
		movedObject.position = Vector3.Lerp(movedObject.position, targetPosition, (moveTime - timeLeft)/moveTime);
		
		//Update the wires between plugs
		UpdateWires();
	}
	
	//Place the object at the target position
	movedObject.position = targetPosition;
	
	//Update the wires between plugs
	UpdateWires();
	
	//We stopped moving
	isMoving = false;
}

//This function unlocks and opens a container. After that the container will have an unlocked lock
function Unlock()
{
	//Set and play relevant sounds
	//audio.Stop();
	if ( soundUnlock )    GetComponent.<AudioSource>().PlayOneShot(soundUnlock);
	
	//Wait for a second
	yield WaitForSeconds(1.5);
	
	//Set the container to unlocked and activate it
	if ( lockParent )
	{
		lockParent.gameObject.SetActive(true);
		lockParent.GetComponent(LHcontainer).locked = false;
		lockParent.GetComponent(LHcontainer).Activate();
	}
	
	//Exit the lock
	Exit();
}

//This function runs when we run out of moves
function Fail()
{
	failure = true;
	
	GetComponent.<AudioSource>().PlayOneShot(soundFail);
	
	//Wait for a second
	yield WaitForSeconds(1);
	
	//Activate the fail functions on the container
	if ( lockParent )
	{
		lockParent.gameObject.SetActive(true);
		lockParent.GetComponent(LHcontainer).FailActivate();
	}
	
	//Exit the lock
	Exit();
}

//This function aborts the lockpicking gameplay and reactivates the activator
function Exit()
{
	if ( currentPlug && currentSlot.Find("Plug") == null )    currentPlug.parent = thisTransform;
	
	//Activate the activator prefab, meaning that we are done with lockpicking
	activator.SetActive(true);
	
	//Enable the container script
	//lockParent.GetComponent(LHcontainer).enabled = true;
	
	//Activate the container object
	lockParent.gameObject.SetActive(true);
	
	//Lock the mouse pointer
	if ( lockCursorOnExit )    Cursor.lockState = CursorLockMode.Locked;
	
	//Destroy this lock
	Destroy(gameObject);
}

function OnDrawGizmos()
{
	//Draw lines between the plugs so we see where the wires are when editing a lock
	for ( var plug in plugs )
	{
		Gizmos.DrawLine( plug.plug.Find("Wire").position, plug.connectsTo.Find("Wire").position);
	}
}

function OnGUI() 
{
    GUI.skin = GUISkin;
    
    //Abort button
	if ( GUI.Button( abortRect, abortText) )
	{
		Exit();
	}
	
	//Display lockpick health
    GUI.Label( movesLeftRect, movesLeftText + "\n" + (movesLeft).ToString());
    
    //Some explanation of how to play
    if ( controlsType == "android" || controlsType == "iphone" )
	{
		GUI.Label( Rect( 0, Screen.height - 60, Screen.width, 60), descriptionMobile);
	
	}
	else
	{
		GUI.Label( Rect( 0, Screen.height - 60, Screen.width, 60), description);
	}
}
