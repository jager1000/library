/*This script handles a standard lock, which requires the activator to rotate a lockpick while turning the cylinder in order to unlock
it. The lockpick may break, and there is a limited number of lockpicks with the activator*/

#pragma strict
#pragma downcast

private var thisTransform:Transform;

//The camera attached to this lock
var cameraObject:Transform;

//The cutter object
var cutterObject:Transform;

//The timer object
var timerObject:Transform;

//The beeper object that will go off when cutting a lose wire
var beeperObject:Transform;

//A list of all the wires in this bomb
var wires:Transform[];

//The currently selected wire
private var currentWire:Transform;

//A list of the indexes of the list of all wires
private var wiresIndexArray:Array;

//The number of win/fail/timer wires available, must not be more than the total number of wires
var winWiresCount:int = 1;
var failWiresCount:int = 1;
var timerWiresCount:int = 1;

//A list of the correct wires, fail wires, and timer wires
internal var winWires:Transform[];
internal var failWires:Transform[];
internal var timerWires:Transform[];

//How many win wires need to be cut in order to win
var cutsToWin:int = 1;

//How many fail wires need to be cut in order to fail
var cutsToFail:int = 1;

//The multiplier for the speed when cutting a timer wire
var timerSpeedChange:float = 1.1;

//How much time is left before we fail
var timeLeft:float = 99;

//How quickly the timer counts
private var timerSpeed:float = 1;

//A list of available wire colors, from which 3 are randomly chosen to represent win/fail/timer wires
var wireColors:Color[];

//The highlight color of the wire when we select it
var wireHighlight:Color;

//Holds the previous color of the selected wire
internal var tempColor:Color;

//The the material of the beeper when it beeps
var beeperMaterial:Material;

//A counter for the beeper
private var beeperCount:float = 1;

//The highlighted shader
private var highlightShader:Shader;
private var tempShader:Shader;

//The speed at which you orbit around the bomb
var rotateSpeed:float = 300;

//The speed at which the bomb will go off
var rotateFailSpeed:float = 3;

//How much time before the bomb goes off if we are rotating it too fast
var rotateFailTime:float = 1;

//How long it takes to cut a wire
var wireCutTime:float = 1;
private var wireCutTimeCount:float = 0;

//Are we cutting the wire now?
private var isCuttingWire:boolean = false;

//Minutes and seconds for the timer
private var minutes:int = 0;
private var seconds:int = 0;

//Did we succeed or did we fail?
private var success:boolean = false;
private var failure:boolean = false;

//The name of the tool in the player's inventory which is required to unlock this lock ( lockpicks, bobbypins, safe cracker tools, etc )
var requiredTool:String = "bomb defusal set";

//The index of the tool in the player's inventory
static var requiredToolIndex:int;

//The container of this lock ( A door, a safe, etc )
internal var lockParent:Transform;

//The activator object
internal var activator:GameObject;

//Various sounds
var soundCut:AudioClip;
var soundBeep:AudioClip;
var soundFail:AudioClip;
var soundUnlock:AudioClip;

//GUI for button graphics
var GUISkin:GUISkin;

//The description for how to play this lock
var description:String = "Click on a wire to cut it. Choose the correct color to disarm the bomb and be careful not to cut \nthe wrong wire. Hold the Middle Mouse Button to rotate the bomb and get a better view of the wires.";

//Holds the type of controls we use, mobile or otherwise
private var controlsType:String = "";

//Should we lock the mouse pointer on Exit?
var lockCursorOnExit:boolean = false;

function Start() 
{
	//Detect if we are running on Android or iPhone
	#if UNITY_IPHONE
    	controlsType = "iphone";
    	print("iphone");
  	#endif
  	
  	#if UNITY_ANDROID
    	controlsType = "android";
    	print("android");
  	#endif
  	
	//Caching the transform for quicker access
	thisTransform = this.transform;
	
	//Deactivate the activator and container objects so they don't interfere with the lockpicking gameplay
	activator.SetActive(false);
	//lockParent.gameObject.SetActive(false);
	
	//If the total number of win/fail/timer wires exceeds the total number of wires, display a warning
	if ( winWiresCount + failWiresCount + timerWiresCount > wires.Length )
	{
		Debug.LogWarning("The number of win/fail/timer wires is larger than the total number of wires available");
	}
	
	//Randomize the wire types (win, fail, timer)
	RandomizeBuiltinArray(wires);
	
	//Randomize the colors of the wires
	RandomizeBuiltinArrayColor(wireColors);
	
	//Make sure we have at least 3 colors for the wire types
	if ( wireColors.Length < 3 )
	{
		Debug.LogWarning("Must have at least 3 wire colors");
	}
	else
	{
		//Go through all the wires
		for ( var wire in wires )
		{
			//Find the already-cut model of the wire and deactivate it
			wire.Find("WireCut").gameObject.SetActive(false);
			
			//Assign the win wires
			if ( winWiresCount > 0 )
			{
				winWiresCount--;
				
				//Set the name
				wire.name = "winWire";
				
				//Set the color
				wire.GetComponent.<Renderer>().material.color = wireColors[0];
				
				//Set the color to the already-cut model of the wire too
				wire.Find("WireCut").GetComponent.<Renderer>().material.color = wireColors[0];
			}
			else if ( failWiresCount > 0 )
			{
				failWiresCount--;
				
				//Set the name
				wire.name = "failWire";
				
				//Set the color
				wire.GetComponent.<Renderer>().material.color = wireColors[1];
				
				//Set the color to the already-cut model of the wire too
				wire.Find("WireCut").GetComponent.<Renderer>().material.color = wireColors[1];
			}
			else if ( timerWiresCount > 0 )
			{
				timerWiresCount--;
				
				//Set the name
				wire.name = "timerWire";
				
				//Set the color
				wire.GetComponent.<Renderer>().material.color = wireColors[2];
				
				//Set the color to the already-cut model of the wire too
				wire.Find("WireCut").GetComponent.<Renderer>().material.color = wireColors[2];
			}
		}
	}
	
	//Set the shader of the highlight
	highlightShader =  Shader.Find("Self-Illumin/Diffuse");
	
	//Put the original shader in a remporary variable
	tempShader = GetComponent.<Renderer>().material.shader;
	
	//Hide the cutter object
	HideCutter(0);
}

//This function randmoizes an array of colors
function RandomizeBuiltinArrayColor(array : Color[])
{
	//Go through all the objects and randmoize them
	for ( var index = array.Length - 1; index > 0; index--) 
	{
		//Choose a random index from the array
		var randomIndex = Random.Range( 0, index);
		
		//Put the object in a temporary variable
		var temp = array[index];
		
		//Put the random index object into the current index object
		array[index] = array[randomIndex];
		
		//Place the temporary object into the random index object
		array[randomIndex] = temp;
	}
}

//This function randmoizes a built-in array
function RandomizeBuiltinArray(array : Object[])
{
	//Go through all the objects and randmoize them
	for ( var index = array.Length - 1; index > 0; index--) 
	{
		//Choose a random index from the array
		var randomIndex = Random.Range( 0, index);
		
		//Put the object in a temporary variable
		var temp = array[index];
		
		//Put the random index object into the current index object
		array[index] = array[randomIndex];
		
		//Place the temporary object into the random index object
		array[randomIndex] = temp;
	}
}

function Update() 
{
	//Show the cursor and allow it to move while we interact with the lock 
	Cursor.lockState = CursorLockMode.None;
	Cursor.visible = true;
	
	//If we have not yet failed or succeeded, keep the game running
	if ( failure == false && success == false )
	{
		//Count down the time left until the bomb explodes
		timeLeft -= Time.deltaTime * timerSpeed;
		
		//Calculate the minutes left
		minutes = Mathf.Floor(timeLeft/60);
		
		//Calculate the seconds left
		seconds = Mathf.Floor(timeLeft - minutes * 60);
		
		//Assign the time in the text mesh
		timerObject.GetComponent(TextMesh).text = minutes.ToString("00") + ":" + seconds.ToString("00") + ":" + (Mathf.Floor(10 * (timeLeft % 1))).ToString("0");//(Mathf.Ceil(timeLeft * 100)/100).ToString("00:00:0");
		
		//If we have less than 10 seconds left, start beeping
		if ( timeLeft <= 10 )
		{
			//Count up the beeper timer
			beeperCount += Time.deltaTime * timerSpeedChange;
			
			//As we get closer to explosion start beeping faster and faster
			if ( timeLeft <= 10 && beeperCount >= 1 )
			{
				HighlightObject(beeperObject, 0.2);
				
				beeperCount = 0;
			}
			else if ( timeLeft <= 5 && beeperCount >= 0.5 )
			{
				HighlightObject(beeperObject, 0.2);
				
				beeperCount = 0;
			}
			else if ( timeLeft <= 2 && beeperCount >= 0 )
			{
				HighlightObject(beeperObject, 0.5);
				
				beeperCount = 0;
			}
		}
		
		//If there is no more time left, fail and set off the bomb
		if ( timeLeft <= 0 )
		{
			Fail();
		}
		
		//If you hold down the middle mouse button, you can rotate the bomb around
		if ( Input.GetButton("Fire3") )
		{
			//Rotate around based on mouse movement
			cameraObject.RotateAround( thisTransform.position, cameraObject.right, Input.GetAxis("Mouse Y") * Time.deltaTime * -rotateSpeed );
			cameraObject.RotateAround( thisTransform.position, cameraObject.up, Input.GetAxis("Mouse X") * Time.deltaTime * rotateSpeed );
			
			//If we rotate the bomb too quickly, start beeping.
			if ( Mathf.Abs(Input.GetAxis("Mouse Y") * Time.deltaTime * -rotateSpeed) > rotateFailSpeed || Mathf.Abs(Input.GetAxis("Mouse X") * Time.deltaTime * rotateSpeed) > rotateFailSpeed )
			{
				rotateFailTime -= Time.deltaTime;
				
				//Highlight the beeper object
				HighlightObject(beeperObject, 0.1);
				
				//If we rotate the bomb for too long, explode
				if ( rotateFailTime <= 0 )
				{
					Fail();
				}
			}
		}
		
		//Check hits with the wires
		if ( isCuttingWire == false )
		{
			var hit:RaycastHit;
		    var ray:Ray = cameraObject.GetComponent.<Camera>().ScreenPointToRay(Input.mousePosition);

			if ( Physics.Raycast( ray, hit, 100) )
		    {
				//Allow hits only with a wire
			    if ( hit.collider.tag == "LHwire" && currentWire == null )
			    {
					//Set the current wire and highlight it
					currentWire = hit.collider.transform;
					currentWire.GetComponent.<Renderer>().material.shader = highlightShader;
					
					//Set the position and rotation of the cutter based on the current wire
					cutterObject.position = currentWire.Find("IconCut").transform.position;
					cutterObject.rotation = Quaternion.LookRotation(cutterObject.position - currentWire.position);
					
				}
			}
			else
			{
				//Clear the highlight off other wires
				for ( var wire in wires )
				{
					if ( wire )    wire.GetComponent.<Renderer>().material.shader = tempShader;
				}
				
				HideCutter(0.2);
			}
		}
	}
	
	//If we have a wire selected...
	if ( currentWire != null )
	{
		//If we click LMB start cutting the wire
		if ( Input.GetButtonDown("Fire1") )
		{
			isCuttingWire = true;
			
			ShowCutter();
		}
		
		//If we are cutting the wire...
		if ( isCuttingWire == true )
		{
			//Count up to the cut
			wireCutTimeCount += Time.deltaTime;
			
			//If we completed the cutting process, check which wire we got
			if ( wireCutTimeCount >= wireCutTime )
			{
				isCuttingWire = false;
				
				//Check if we got a win/lose/timer wire
				if ( currentWire.name == "winWire" )
				{
					cutsToWin--;
					
					//If we cut enough win wires, unlock the bomb
					if ( cutsToWin <= 0 )
					{
						Unlock();
					}
				}
				else if ( currentWire.name == "failWire" )
				{
					cutsToFail--;
					
					//If we cut enough fail wires, explode the bomb
					if ( cutsToFail <= 0 && failure == false )
					{
						Fail();
					}
				}
				else if ( currentWire.name == "timerWire" )
				{
					//If we cut a timer wire, change timer speed
					timerSpeed *= timerSpeedChange;
				}
				
				//Replace the wire with an already-cut wire object
				currentWire.Find("WireCut").gameObject.SetActive(true);
				currentWire.Find("WireCut").parent = currentWire.parent;
				
				//Play a cut sound
				GetComponent.<AudioSource>().PlayOneShot(soundCut);
				
				//Remove the current wire
				Destroy(currentWire.gameObject);
				
				//Hide the cutter object
				HideCutter(0.2);
			}
		}
	}
	
	//If we unclick LMB, cancel the cutting process
	if ( Input.GetButtonUp("Fire1") )
	{
		HideCutter(0);
	}
}

//This function shows the cutter object
function ShowCutter()
{
	if ( currentWire )    
	{
		//Hide the wire icon
		currentWire.Find("IconCut").gameObject.SetActive(false);
	
		//Rotate the wire cutter to align correctly with the wire
		//cutterObject.RotateAround(dsdsd,dsds);	
	}
	
	//Show the cutter object
	cutterObject.gameObject.SetActive(true);
	
	
	
	//Play the cutter animation
	cutterObject.GetComponent.<Animation>().Play();
}

//This function hides the cutter object
function HideCutter( delay:float )
{
	//Show the wire icon
	if ( currentWire )    currentWire.Find("IconCut").gameObject.SetActive(true);
	
	//Reset the cutting timer
	wireCutTimeCount = 0;
	
	//No wire is selected
	currentWire = null;
	
	//We are not cutting a wire
	isCuttingWire = false;
	
	//Wait a little
	yield WaitForSeconds(delay);
	
	//And hide the cutter object
	if ( cutterObject.gameObject.activeSelf == true )
	{
		cutterObject.gameObject.SetActive(false);
	}
}

//This function highlights an object by replacing its shader
function HighlightObject( target:Transform, beepTime:float )
{
	//Set the highlighted shader
	target.GetComponent.<Renderer>().material.shader = highlightShader;
	
	//Play a beeping sound
	GetComponent.<AudioSource>().PlayOneShot(soundBeep);
	
	//Wait for a while
	yield WaitForSeconds(beepTime);
	
	//Reset the back to the normal shader
	if ( target )    target.GetComponent.<Renderer>().material.shader = tempShader;
}

//This function unlocks and opens a container. After that the container will have an unlocked lock
function Unlock()
{
	success = true;
	
	timerSpeed = 0;
	
	//Set and play relevant sounds
	GetComponent.<AudioSource>().Stop();
	GetComponent.<AudioSource>().PlayOneShot(soundUnlock);
	
	//Wait for a second
	yield WaitForSeconds(2);
	
	//Set the container to unlocked and activate it
	if ( lockParent )
	{
		lockParent.gameObject.SetActive(true);
		lockParent.GetComponent(LHcontainer).locked = false;
		lockParent.GetComponent(LHcontainer).Activate();
	}
	
	//Exit the bomb difuse game
	Exit();
}

//This function runs when we fail to disarm the bomb
function Fail()
{
	failure = true;
	
	timerSpeed = 0;
	
	timerObject.GetComponent(TextMesh).text = ("BYE BYE").ToString();
	
	GetComponent.<AudioSource>().PlayOneShot(soundFail);
	
	//Wait for a second
	yield WaitForSeconds(1);
	
	//Activate the fail functions on the container
	if ( lockParent )
	{
		lockParent.gameObject.SetActive(true);
		lockParent.GetComponent(LHcontainer).FailActivate();
	}
	
	//Exit the bomb difuse game
	Exit();
}

//This function aborts the lockpicking gameplay and reactivates the activator
function Exit()
{
	//Activate the activator prefab, meaning that we are done with lockpicking
	activator.SetActive(true);
	
	//Enable the container script
	//lockParent.GetComponent(LHcontainer).enabled = true;
	
	//Activate the container object
	lockParent.gameObject.SetActive(true);
	
	//Lock the mouse pointer
	if ( lockCursorOnExit )    Cursor.lockState = CursorLockMode.Locked;
	
	//Destroy this lock
	Destroy(gameObject);
}

function OnGUI() 
{
    GUI.skin = GUISkin;
    
    //Some explanation of how to play
    GUI.Label( Rect( 0, Screen.height - 60, Screen.width, 60), description);
}