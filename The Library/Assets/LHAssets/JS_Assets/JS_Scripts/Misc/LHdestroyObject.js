﻿//This script runs a Destroy function on a target after a delay. If no target it set, the function runs on this gameobject
#pragma strict

var destroyTarget:Transform;

var delay:float = 0;


function Start() 
{

}

function Update() 
{

}

function DestroyObject()
{
	//Destroy the target, if it exists. Otherwise, destroy this game object
	if ( destroyTarget )    Destroy( destroyTarget.gameObject, delay);
	else    Destroy( gameObject, delay);
}