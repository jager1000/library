﻿//This script creates effects at a position, and removes them after a while
#pragma strict

private var thisTransform:Transform;

var removeAfter:float = 2;

//List of effects to be created
var effects:LHeffects[];

class LHeffects 
{
    //The effect object, where to create it at, and after how many seconds to remove it
    var effect:Transform;
    var createAt:Transform;
	var removeAfter:float = 2;
}

function Start() 
{
	thisTransform = transform;
}


//This function creats the effects
function CreateEffect()
{
	//Go through all effects
	for ( var effect in effects )
	{
		//Create a new effect at the correct position
		if ( effect.createAt )
		{
			var newEffect = Instantiate( effect.effect, effect.createAt.position, effect.createAt.rotation);
		}
		else
		{
			newEffect = Instantiate( effect.effect, Camera.main.transform.position, Camera.main.transform.rotation);
		}
		
		//Set the remove time
		if ( effect.removeAfter > 0 )
		{
			Destroy(newEffect.gameObject, effect.removeAfter);
		}
	}
	
	//Set the remove time for this object (the object that creates the effects)
	if ( removeAfter > 0 )
	{
		Destroy(thisTransform.gameObject, removeAfter);
	}
}