#pragma strict

//GUI for button graphics
var GUISkin:GUISkin;

function OnGUI() 
{
    GUI.skin = GUISkin;
    
    //Some explanation of how to play
    GUI.Label( Rect( 0, Screen.height - 40, Screen.width, 40), "Press A/D to move left/right. Click the Left Mouse Button to interact with objects");
}