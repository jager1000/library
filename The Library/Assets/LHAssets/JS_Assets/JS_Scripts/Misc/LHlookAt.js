﻿//This script makes an object look at another. Used mainly to make billboard style objects that always align with the camera.
#pragma strict

private var thisTransform:Transform;

//The object to look at
var lookAtObject:Transform;

function Start() 
{
	thisTransform = this.transform;
}

function Update() 
{
	//Keep looking at the object
	thisTransform.LookAt(lookAtObject);
}