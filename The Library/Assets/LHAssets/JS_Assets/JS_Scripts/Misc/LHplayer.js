/*This script is just a simple representation of a player character, allowing you to move left/right*/

#pragma strict

private var thisTransform:Transform;

//The player's speed
var speed:float = 10;

//Holds the type of controls we use, mobile or otherwise
private var controlsType:String = "";

function Start() 
{
	//Detect if we are running on Android or iPhone
	#if UNITY_IPHONE
    	controlsType = "iphone";
    	print("iphone");
  	#endif
  	
  	#if UNITY_ANDROID
    	controlsType = "android";
    	print("android");
  	#endif
  	
  	//Cache this transform for quicker access
	thisTransform = transform;
}

function FixedUpdate()
{
	//If we are using Android/iPhone, controls are touch based
	if ( controlsType == "android" || controlsType == "iphone" )
	{
		//If we are touching the screen
		if( Input.touchCount > 0 )
		{
			//If we are swiping the screen horizontally
			if ( Mathf.Abs(Input.GetTouch(0).deltaPosition.x) > 0.5 )
			{
				//Moving horizontally and vertically with touch screen swipes
				thisTransform.Translate( Vector3.right * speed * 0.2 * Input.GetTouch(0).deltaPosition.x * Time.deltaTime, Space.World );
			}
		}
	}
	else
	{
		//Moving horizontally and vertically with WASD
		thisTransform.Translate( -Vector3.right * speed * Input.GetAxis("Horizontal") * Time.deltaTime, Space.World );
	}
}