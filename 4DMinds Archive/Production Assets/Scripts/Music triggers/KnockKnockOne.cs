﻿using UnityEngine;
using System.Collections;

public class KnockKnockOne : MonoBehaviour {

	public KnockKnockTwo knock;
	private bool goodToGo = false;

	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Player")){
			knock.hasTriggered = true;
			goodToGo = true;
		}
	}

	void Update(){
		if (goodToGo) {
			Destroy(gameObject);
		}
	}

}
