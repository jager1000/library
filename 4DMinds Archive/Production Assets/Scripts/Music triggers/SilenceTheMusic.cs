﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class SilenceTheMusic : MonoBehaviour {

	public AudioMixerSnapshot silence;

	private bool m_HasTriggered = false;
	private float m_Transition = 25f;
	
	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player") && m_HasTriggered == false)
		{
			silence.TransitionTo(m_Transition);
			m_HasTriggered = true;
		}
	}

}
