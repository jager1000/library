﻿using UnityEngine;
using System.Collections;

public class KnockKnock : MonoBehaviour {


	private AudioSource source;
	public AudioClip knocking;
	private bool delete = false;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
	}

	public void PlayKnock(){
		source.PlayOneShot (knocking);
		delete = true;
	}
}