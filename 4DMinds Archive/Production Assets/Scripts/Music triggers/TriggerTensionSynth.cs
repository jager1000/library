﻿using UnityEngine;
using System.Collections;

public class TriggerTensionSynth : MonoBehaviour {


	private bool played = false;
	private AudioSource source;
	public AudioClip spotted;


	void Start(){
		source = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider other){
		
		if(other.CompareTag("Player") && played == false){
			GetComponent<AudioSource>().Play();
			played = true;

		}
	}

	public void PlaySpotted(){
		source.PlayOneShot (spotted, 1f);
	}
}
