﻿using UnityEngine;
using System.Collections;

public class OutsideNoise : MonoBehaviour {

	public AudioClip atmosphere;
	private AudioSource source;

	void Awake(){

		source = GetComponent<AudioSource>();
	}

	void OnTriggerEnter(Collider other){

		if(other.CompareTag("Player")){
			source.PlayOneShot(atmosphere, 1F);
		}
	}

	void OnTriggerExit(Collider other){

		if(other.CompareTag("Player")){
			source.Stop();
		}
	}

}
