﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class ExitLibrary : MonoBehaviour {

	public AudioMixerSnapshot outside;
	private float m_FadeIn = 3f;

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player")) {
			outside.TransitionTo (m_FadeIn);
		}
	}
}
