﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class EnterLibrary : MonoBehaviour {

	public Light lightOne;
	public Light lightTwo;
    public Light lightThree;
    public Light DirectionalLight;
	public ParticleSystem lightOnePartical;
	public ParticleSystem lightTwoPartical;

	public WindowSlam window;
	public NearWindow nearWindow;

	public AudioMixerSnapshot musicFadeIn;
	public AudioClip music;
	public AudioClip blowOutCandles;
	public AudioSource musicSource;
	public AudioSource sfxSource;

	private bool m_HasTriggered = false;
	private float m_MusicFadeIn = 30f;



	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			if (m_HasTriggered == false)
			{
				musicSource.PlayOneShot(music);
				musicSource.PlayDelayed(120f);
				musicFadeIn.TransitionTo(m_MusicFadeIn);
				m_HasTriggered = true;
				sfxSource.PlayOneShot(blowOutCandles, 0.75f);
				window.CloseWindow();
				nearWindow.NowInside();
			}
		}
	}


	void Update(){
		if (m_HasTriggered && lightOne.intensity > 0f) {
			lightOne.intensity -= 5f * Time.deltaTime;
			lightTwo.intensity -= 5f * Time.deltaTime;
            lightThree.intensity -= 5f * Time.deltaTime;
            DirectionalLight.intensity -= 5f * Time.deltaTime;
		}
		if (lightOne.intensity == 0f && lightTwo.intensity == 0f) {
			lightOnePartical.enableEmission = false;
			lightTwoPartical.enableEmission = false;
		}
	}


}
