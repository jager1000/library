﻿using UnityEngine;
using System.Collections;

public class HallwayScareTrigger : MonoBehaviour {

	public HallwayScare scare;
	bool delete = false;

	void OnTriggerEnter(Collider other){
		scare.triggered = true;
		delete = true;
	}

	void Update(){
		if (delete) {
			Destroy(gameObject);
		}
	}
}
