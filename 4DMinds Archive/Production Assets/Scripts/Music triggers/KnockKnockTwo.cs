﻿using UnityEngine;
using System.Collections;

public class KnockKnockTwo : MonoBehaviour {

	public bool hasTriggered = false;
	public KnockKnock knockKnock;
	private bool delete = false;

	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Player") && hasTriggered) {
			knockKnock.PlayKnock ();
			delete = true;
		}
	}

	void Update(){
		if (delete) {
			Destroy (gameObject);
		}
	}
}
