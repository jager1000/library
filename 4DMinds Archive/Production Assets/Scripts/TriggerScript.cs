﻿using UnityEngine;
using System.Collections;

public class TriggerScript : MonoBehaviour {

	public bool hitTrigger = false;
	public bool triggerSpent = false;

	void OnTriggerEnter(Collider detectedObject){
		if (triggerSpent == false) {
			if (detectedObject.tag == ("Player")){
				hitTrigger = true;
				triggerSpent = true;
			}
		}
	}
}
