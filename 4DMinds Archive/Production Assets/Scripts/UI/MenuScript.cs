﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuScript : MonoBehaviour {

    public GameObject startMenu;
    public GameObject quitMenu;
    public GameObject warningMenu;
    public GameObject startScene;
    public GameObject loadLevel;
    public GameObject cont;
    public GameObject loadingImage;
    public GameObject story;
    public GameObject nextButton;
    public GameObject wearHeadphones;
	public GameObject creditsMenu;

	public AudioClip click;
	public AudioClip exit;
	private AudioSource source;

	// Use this for initialization
	void Start () {
        loadLevel.SetActive(false);
		source = GetComponent<AudioSource> ();
	}
	
	public void ExitPress(){
        quitMenu.SetActive(true);
        startMenu.SetActive(false);
		source.PlayOneShot (click);
	}

	public void NoPress(){
        quitMenu.SetActive(false);
        startMenu.SetActive(true);
		source.PlayOneShot (click);
	}

	public void StartLevel(){
        startScene.SetActive(false);
        warningMenu.SetActive(true);
		source.PlayOneShot (click);
	}

    public void nextPress(){
        wearHeadphones.SetActive(false);
        story.SetActive(true);
        cont.SetActive(true);
        nextButton.SetActive(false);
		source.PlayOneShot (click);
    }

    public void continuePress(){
		source.PlayOneShot (click);
        loadingImage.SetActive(true);
        cont.SetActive(false);
        Application.LoadLevel(1);
    }

	public void CreditsPress(){
		source.PlayOneShot (click);
		creditsMenu.SetActive (true);
	}

	public void DonePress(){
		creditsMenu.SetActive(false);
		startMenu.SetActive(true);
		source.PlayOneShot (click);
	}

	public void ExitGame(){
		source.PlayOneShot (exit);
        System.Diagnostics.Process.GetCurrentProcess().Kill();
		Application.Quit ();
	}
}
