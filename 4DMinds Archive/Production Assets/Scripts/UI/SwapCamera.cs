﻿using UnityEngine;
using System.Collections;

public class SwapCamera : MonoBehaviour {

    public Camera camera1;
    public Camera camera2;
    public GameObject activate;
    public GameObject deactivate;

    void Start () {
        
    }

    void Update () {
        
    }
    void OnTriggerEnter(Collider other){
        if(other.CompareTag("activate")){
            camera1.enabled = false;
            camera2.enabled = true;
            camera2.GetComponent<LerpAtoB>().enabled = true;
            this.enabled = false;
        }
    }
}
