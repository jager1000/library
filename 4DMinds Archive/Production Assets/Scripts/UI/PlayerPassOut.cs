﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Audio;

public class PlayerPassOut : MonoBehaviour {
    
    public Image image;
    public GameObject fadeInImage;
    public float fadeIn = 0.5f;
    public bool switchUp = false;
    
    void Start () {
        image.enabled = true;
    }
    
    // Update is called once per frame
    void Update () {
        Color color = GetComponent<CanvasRenderer>().GetColor();
        if(switchUp == true){
            if(color.a <= 200f){
                color.a += fadeIn * Time.deltaTime;
                GetComponent<CanvasRenderer>().SetColor(color);
            }
            switchUp = false;
        }
        if(switchUp == false){
            if(color.a >= 200f){
                color.a -= fadeIn * Time.deltaTime;
                GetComponent<CanvasRenderer>().SetColor(color);
            }
            if(color.a <= 0.1f){
                image.enabled = false;
              }
            }
         }
}
