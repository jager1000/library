﻿using UnityEngine;
using System.Collections;

public class SwapCameraBack : MonoBehaviour {

    public Camera camera1;
    public Camera camera2;

    void Start(){}
    void Update(){}

    void OnTriggerEnter(Collider other){
        if(other.CompareTag("deactivate")){
            print("hit");
            camera1.enabled = true;
            camera2.enabled = false;

        }
    }

}
