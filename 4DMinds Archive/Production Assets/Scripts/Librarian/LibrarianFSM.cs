﻿using UnityEngine;
using System.Collections;

public class LibrarianFSM : MonoBehaviour {


	public int index = 0;
    public Transform playerPos;
	public GameObject[] wayPoint;
	public GameObject[] findSmallCrumb;
	public GameObject[] findLargeCrumb;
    public AnimationClip Attack;
    public AnimationClip Death;
    public AnimationClip Idle;
    public AnimationClip Walk;
    private Animation anim;
    public GameObject SpawnPoint1;
    public GameObject SpawnPoint2;
    public AudioClip proximityNoise;
    public AudioClip attention;
    public AudioClip passive;
    public AudioClip searching;
    public AudioClip spotted;
    public static bool isAlive = true;
    public GameObject bloodSplatter;

    private int state = 1;
    private int[,] currentState;
    private const int TOUCHING = 2;
    private Vector3 currentPos;
    private NavMeshAgent librarian;
    private AudioSource source;
    private SoundPlaceHolder soundHolder;
    private float normalPace;
    private float fastPace;
    private float superFastPace;

    void Start () {
        //ignore these lines
		currentState = StateMatrix.stateMachine;
		librarian = GetComponent<NavMeshAgent>();
        source = GetComponent<AudioSource>();
        soundHolder = GameObject.FindGameObjectWithTag("SoundHolder").GetComponent<SoundPlaceHolder>();
        bloodSplatter.SetActive(false);

        //this is the part that talks to the Animation component of the model, if you were to type anim. <- the dot should start showing options for
        //things you can manipulate or play ect. Look up Animation in the unity library for list of options
        anim = GetComponent<Animation>();

        //the librarians speed changers depending on how close she is to the player and different states, you can see below where it changes.
        fastPace = librarian.speed + 2;
        superFastPace = librarian.speed + 5;
        normalPace = librarian.speed;

	}

	void Update () {
        //This looks for anything in the scene with the tag Waypoint and helps her map them, also the players sprinting trail.
        wayPoint = GameObject.FindGameObjectsWithTag("Waypoints");
		findLargeCrumb = GameObject.FindGameObjectsWithTag("LongLifeBread");
		findSmallCrumb = GameObject.FindGameObjectsWithTag("BreadCrumb");
        currentPos = transform.position;
        //print(state);

        //Below in the code you will see me calling state = 2 while in Wandering(){} ect this is what changes her state and behaviour.
		switch (state) {
		case 1:
			Wandering();
			break;
		case 2:
			Alerted();
			break;
		case 3:
			Searching();
			break;
		case 4:
			Attacking();
			break;
        case 5:
            Dead();
            break;
		default:
			print ("Critical error");
			break;
		}
	}

    /// <summary>
    /// You can ignore this method, I might remove it later.
    /// </summary>
    /// <param name="nextState">Next state.</param>
	void updateState (int nextState)	{
		if (currentState [state, nextState] == -1 && currentState [state, nextState] != 0) {
			print ("neg 1 state");
		} else {
			state = currentState [state, nextState];
			print (state);
		}
	}

    /// <summary>
    /// The state she uses when just going from point to point
    /// </summary>
	void Wandering(){
        //Setting the walking speed to normal
        librarian.speed = normalPace;
        //Playing the idle animation from the clip which is just a set amount of frames. Feel free to change this if it's wrong.
        anim.Play("Idle");

        //If her current position is less than 10 metres from the player go into the Searching state immediately.
        if(Vector3.Distance(currentPos, playerPos.position) <= 10){
            state = 3;
        }
        //This just controls which waypoint she goes to next so its random, don't have to change this 
        if(wayPoint[index%wayPoint.Length]){
			librarian.SetDestination(wayPoint[index%wayPoint.Length].transform.position);
		}
        //Move to the next way point
        if(Vector3.Distance(currentPos,wayPoint[index%wayPoint.Length].transform.position) <= 3){
			index++;
		}
        //Looking for player made noise if find crumbs go into alerted state
        if(findSmallCrumb.Length > 0){
            source.PlayOneShot(soundHolder.Passive[0], 1.0f);
            state = 2;
		}
        //Looking for another version of player made noise if find crumbs go into alerted state
        if(findLargeCrumb.Length > 0){
            source.PlayOneShot(soundHolder.Passive[1], 1.0f);
            state = 2;
		}

	}

    /// <summary>
    /// Basically if the player has been detected
    /// </summary>
	void Alerted(){
        //Switch from current animation to idle immediately.
        anim.Play("Walk");
        //Increase speed
        librarian.speed = fastPace;
        //If less than 10m from the player switch to Searching state below
        if(Vector3.Distance(currentPos, playerPos.position) <= 10){
            state = 3;
        }
        //Same as above
        if (findLargeCrumb.Length > 0) {
			librarian.SetDestination(findLargeCrumb[0].transform.position);
		}
        //Same as above
        if(findSmallCrumb.Length > 0){
			librarian.SetDestination(findSmallCrumb[0].transform.position);
		}else{
            //Play a sounds of frustration and return to normal if she loses the player, switch to idle
            source.PlayOneShot(soundHolder.Attenton[6], 1.0f);
            state = 1;
		}
	}
    /// <summary>
    /// Will scope around for the player if detected
    /// </summary>
	void Searching(){
        //Change speed
        librarian.speed = fastPace;
        //If the player is less than 7m away she will dive for them
        if (Vector3.Distance(currentPos, playerPos.position) <= 7){
            librarian.SetDestination(playerPos.position);
        }
        //Once she gets within 4 metres of the player she will switch to state 4 attacking, its pretty much over at this point
        if (Vector3.Distance(currentPos, playerPos.position) <= 4){
            source.PlayOneShot(soundHolder.Spotted[0], 1.0f);
             state = 4;
            }
        //If at any time the player gets greater than 10m away again she will switch to idle and play a sound.
        if (Vector3.Distance(currentPos, playerPos.position) >= 10){
            source.PlayOneShot(soundHolder.Attenton[5], 1.0f);
            state = 1;
            }
        }

    /// <summary>
    /// Attacking the player
    /// </summary>
	void Attacking(){
        //Change to attack animation
        anim.Play("Attack");
        //Top speed.
        librarian.speed = superFastPace;
        //Go directly to the players position
        librarian.SetDestination(playerPos.position);
        //This is relative to another script the PlayerManager and the whole breath hiding mechanic whilst holding Q, I will need to go into more depth later.
        //but basically if the player is holding breath she will go into idle again
        if (PlayerManager.isHidden == true){
            source.PlayOneShot(soundHolder.Attenton[6], 1.0f);
            state = 1;
        }
        //If she comes in contact with the player at the moment it is just resetting the level, I have commented it out. Feel free to change this to maybe
        //Moving the player somewhere instead who knows. This also sets her to idle again.
		if(Vector3.Distance(currentPos,playerPos.position) <= TOUCHING){
            //Application.LoadLevel(1);
            state = 1;
		}
        //At this point if the player gets 15m away set the position set to idle and normal speed, play sound.
        if(Vector3.Distance(currentPos, playerPos.position) >= 15){
            librarian.speed = normalPace;
            source.PlayOneShot(soundHolder.Attenton[6], 1.0f);
            state = 1;
        }
    }

    /// <summary>
    /// Only used in the old version
    /// </summary>
    void Dead(){
        //This just makes her make a death sound, play a blood splatter, and play the death animation once. and turns her off.
        if (isAlive == true){
            bloodSplatter.SetActive(true);
            source.PlayOneShot(soundHolder.Scream, 0.5f);
            anim ["Death"].wrapMode = WrapMode.Once;
            anim.Play("Death");
            isAlive = false;
        }
    }

    /// <summary>
    /// This was just for the throwing book mechanic so she destroyed it when she touched it. Could be used in other ways.
    /// </summary>
    /// <param name="collision">Collision.</param>
    void OnCollisionEnter(Collision collision){
        if(collision.gameObject.tag == "Book" || collision.gameObject.tag == "BookField"){
            Destroy(collision.gameObject);
        }
    }

    /// <summary>
    /// This just removed the breacrumbs and changes state based on the breathing radius.
    /// Breathing radius and hiding radius were sphere collider game objects basically holding or releasing Q would change between them, the only problem with this is
    /// stuff like the ladder treats the normal radius as the player and the player can climb it from 15m away, it's fine if we activated it once inside maybe.
    /// I would also set it to a different key in the script and also stop the player from being able to move or move quickly whilst holding breath.
    /// </summary>
    /// <param name="other">Other.</param>
	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "BreadCrumb" || other.gameObject.tag == "LongLifeBread"){
			Destroy(other.gameObject);
		}
        if (other.gameObject.tag == "BreathingRadius"){
            state = 3;
        }
        if (other.gameObject.tag == "HidingRadius"){
            state = 1;
        }
        if (other.gameObject.tag == "HidingRadius" && state == 3){
            state = 1;
        }
	}

}
