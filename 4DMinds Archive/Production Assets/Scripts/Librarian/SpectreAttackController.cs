﻿using UnityEngine;
using System.Collections;

public class SpectreAttackController : MonoBehaviour {
//This Script give the abilty to change the spectre's attack range and play's the animation through the controller
//I will create a new attack animation to fit this - im thinking a swipe with claws - the current animation will play on player colllide

	public float attackRadius;
//	private SphereCollider attackCollider;
	public Animator anim;
	public bool SpectreAttack = true;

	// Use this for initialization

	void Start () {
//		attackCollider = transform.GetComponent<SphereCollider> ();	//collider is a capsule so dropped this code
//		attackCollider.radius = attackRadius;

		anim.SetLayerWeight (1, 1);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider detectedObject)
	{
		if (detectedObject.tag == ("Player")){

			if (transform.tag == ("Spectre")){
				anim.SetTrigger("SpectreAttack");
			}else{
				anim.SetTrigger("SpectreSwipe");
			}
		}
	}
}
