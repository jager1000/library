﻿using UnityEngine;
using System.Collections;

public class SpectreAnimation : MonoBehaviour {
// This script controls the speed variable of the animation controller

	public float spectreSpeed;
	private Vector3 previous;
	private Animator anim;
	private GameObject player;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		//finds velocity and assigns to animator every frame
//		spectreSpeed = spectreBody.velocity.magnitude;
		if (previous != transform.position) {
			spectreSpeed = ((transform.position - previous).magnitude) / Time.deltaTime;
			previous = transform.position;
			anim.SetFloat ("SpectreSpeed", spectreSpeed);
		} else {
			spectreSpeed = 0;
		}
	}
}