﻿using UnityEngine;
using System.Collections;

public class TheRealJumpScare : MonoBehaviour {

	public DoorScript door;
	public GameObject librarian;
	public bool otherTriggered = false;
	private AudioSource source;
	private bool hasTriggered = false;

	public Light lantern;
	public Light roomLight;
	private bool hasBlackout = false;

	void Start(){
		source = GetComponent<AudioSource> ();
	}

	void OnTriggerStay(Collider other){
		if (other.CompareTag ("Player") && door.open && otherTriggered && !hasTriggered) {
			librarian.SetActive(true);
			source.PlayDelayed(0.3f);
			hasTriggered = true;
			Destroy(gameObject, 34f);
		}
	}

	void Update(){
		if (source.isPlaying && lantern.intensity > 0f && !hasBlackout) {
			lantern.intensity -= 0.3f * Time.deltaTime;
			roomLight.intensity -= 0.6f * Time.deltaTime;
		}
		else if (source.isPlaying && lantern.intensity <= 0f){
			hasBlackout = true;
		}
		if (hasBlackout && lantern.intensity < 1f){
			lantern.intensity += 0.2f * Time.deltaTime;
			roomLight.intensity += 0.4f * Time.deltaTime;
		}
	}

}
