﻿using UnityEngine;
using System.Collections;

public class CandleFlicker : MonoBehaviour {

	public Light candleLight;
	public float flickerSpeed;
    public float minRange;
    public float maxRange;

	void Start(){
        minRange = candleLight.range - 50f;
        maxRange = candleLight.range + 50f;

	}

	void Update(){
		candleLight.range = Mathf.Lerp (candleLight.range, Random.Range (minRange, maxRange),
		                                flickerSpeed * Random.Range (1, 3) * Time.deltaTime);
	}
}
