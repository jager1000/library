#pragma strict

var chController : Transform;
var inside : boolean = false;
var heightFactor : float = 0.000001;

private var FPSInput : CharacterController;

function Start(){
	FPSInput = GetComponent(CharacterController);
}

function OnTriggerEnter(Col : Collider){
	if(Col.gameObject.tag == "Ladder"){
		inside = !inside;
	}
}

function OnTriggerExit(Col : Collider){
	if(Col.gameObject.tag == "Ladder"){
		inside = !inside;
	}
}
		
function Update(){
	if(inside == true && Input.GetKey("w")){
			chController.transform.position += Vector3.up / heightFactor;
	}
}