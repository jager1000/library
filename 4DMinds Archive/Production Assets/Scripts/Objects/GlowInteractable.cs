﻿using UnityEngine;
using System.Collections;

public class GlowInteractable : MonoBehaviour {
	
		public Color colourStart = Color.yellow;
		public Color colourEnd = Color.white;
		public float duration = 1F;
		private Renderer rend;
		
	void Start() {
		rend = GetComponent<Renderer>();
	}


	void Update() {
		float lerp = Mathf.PingPong(Time.time, duration) / duration;
		rend.material.color = Color.Lerp(colourStart, colourEnd, lerp);
	}



}


//	private Light halo;
//
//	// Use this for initialization
//	void Start () {
//	
//		halo = GetComponent<Light>();
//
//	}
//
//
//	IEnumerator HaloPulse() {
//		yield return new WaitForSeconds(2);
//		halo.range += 0.01f;
//		}
//
//
//	
//	// Update is called once per frame
//	void Update () {
//		if (halo.range == 0f) {
//			StartCoroutine (HaloPulse ());
//		}
//
//		while (halo.range > 0f && halo.range < 0.15f){
//			halo.range += 0.02f;
//		}
//
//		gameObject.
//
//	}
//}
