﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {

    public int index = -1;
    public bool open = false;
    public float doorOpenAngle = 90f;
    public float doorCloseAngle = 0f;
    public float smooth = 2f;

	public AudioClip[] openDoor;
	public AudioClip[] closeDoor;
	public AudioClip doorLocked;
	public AudioClip openSecretDoor;

	private AudioSource source;

	void Start(){
		source = GetComponent<AudioSource>();
	}

	void Update () {
        if(open){
            Quaternion targetRotation = Quaternion.Euler(0,doorOpenAngle,0);
            transform.localRotation = Quaternion.Slerp(transform.localRotation,targetRotation,smooth * Time.deltaTime);
        }else{
            Quaternion targetRotation2 = Quaternion.Euler(0,doorCloseAngle,0);
            transform.localRotation = Quaternion.Slerp(transform.localRotation,targetRotation2,smooth * Time.deltaTime);
        }
	}

    public void ChangeDoorState(){
        
		if (open) {
			source.PlayOneShot (closeDoor [Random.Range (0, closeDoor.Length)]);
		} else {
			source.PlayOneShot (openDoor [Random.Range (0, openDoor.Length)]);
		}
		open = !open;
    }

	public void PlayLockedNoise(){
		source.PlayOneShot (doorLocked);
	}

	public void PlaySecretDoorNoise(){
		source.PlayOneShot (openSecretDoor);
	}

}
