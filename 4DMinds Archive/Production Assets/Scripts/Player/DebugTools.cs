﻿using UnityEngine;
using System.Collections;

public class DebugTools : MonoBehaviour {

    public GameObject[] spawnPos;
    public GameObject player;
    private int index = 0;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyUp(KeyCode.I)){
            AcessAllItems();
        }
        if(Input.GetKeyUp(KeyCode.O)){
            OpenAllDoors();
        }
        if(Input.GetKeyUp(KeyCode.P)){
            player.transform.position = spawnPos[index].transform.position;
            index++;
            if(index > 3){
                index = 0;
            }
        }
	}

    void AcessAllItems(){
        for(int i = 0; i < Inventory.notes.Length; i++){
            Inventory.notes[i] = true;
        }
        for(int i = 0; i < Inventory.keys.Length; i++){
            Inventory.keys[i] = true;
        }
        for(int i = 0; i < Inventory.objectives.Length; i++){
            Inventory.objectives[i] = true;
        }

    }

    void OpenAllDoors(){
        GameObject[] MasterKey = GameObject.FindGameObjectsWithTag("Door");
        for(int i = 0; i < MasterKey.Length; i++){
            MasterKey[i].GetComponentInParent<DoorScript>().open = true;
        }

    }
}
