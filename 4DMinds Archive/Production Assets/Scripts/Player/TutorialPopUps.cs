﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialPopUps : MonoBehaviour {

	public Text tutorialText;

	void Start(){
		tutorialText.enabled = false;

	}

	// Use this for initialization
	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player")) {
			tutorialText.enabled = true;
		}
	}

	void OnTriggerExit(Collider other){
		if (other.CompareTag ("Player")) {
			tutorialText.enabled = false;
		}
	}
}
