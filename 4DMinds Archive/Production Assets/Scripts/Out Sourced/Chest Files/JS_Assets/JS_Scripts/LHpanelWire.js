﻿/*This script defines a panel wire, which is used in the panel lock type. The panel wire check for collision with any other panel wires and changes the color
of the wire accordingly. In the panel lock, part of the game goal is to make sure no wires are intersecting*/

#pragma strict

//The tag of the panel wire
var panelWireTag:String = "LHpanelWire";

//The color of the wire when intersecting with another wire
var wrongWireColor:Color;
private var defaultWireColor:Color;

//Is the wire intersecting?
internal var isIntersecting:boolean = false;

function Start() 
{
	//Set the default color of the wire
	defaultWireColor = GetComponent.<Renderer>().material.GetColor("_Emission");
}

//Check if the wire starts colliding with other wires
function OnTriggerEnter(other:Collider)
{
	if ( other.tag == panelWireTag )
	{
		//Change the color fo the wire to the intersecting color
		GetComponent.<Renderer>().material.SetColor("_Emission", wrongWireColor);
		
		//This wire is intersecting with another wire
		isIntersecting = true;
	}
}

//Check if the wire stops colliding with other wires
function OnTriggerExit(other:Collider)
{
	if ( other.tag == panelWireTag )
	{
		//Change the color fo the wire to the default color
		GetComponent.<Renderer>().material.SetColor("_Emission", defaultWireColor);
		
		//This wire is not intersecting with another wire
		isIntersecting = false;
	}
}