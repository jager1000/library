/*This simple script handles an overhead camera following an object at an offset*/

#pragma strict

private var thisTransform:Transform;

//The object to be followed
var target:Transform;

//How far from the object the camera hovers
var offset:Vector3;

//How fast the camera follows the target
var followSpeed:float = 5;

//Should the camera look at the target?
var lookAtTarget:boolean = true;

function Start() 
{
	//Caching this trasform for quicker access
	thisTransform = this.transform;
	
}

function Update() 
{
	if ( target )
	{
		//Follow this object at an offset
		thisTransform.position = Vector3.Slerp( thisTransform.position, target.position + offset, followSpeed * Time.deltaTime);
		
		//Look at the followed object
		if ( lookAtTarget )    thisTransform.LookAt(target.position);
	}
	else
	{
		Debug.LogWarning("Warning! No target set for camera to follow. Pick a target such as the player trasnform.");
	}
}