﻿using UnityEngine;
using System.Collections;

// This class turns the lights off and changes the bookshelves to their 'maze' position
public class ShiftMazeScript : MonoBehaviour {

	private bool shiftDone = false;		//this script is only done once
	private GameObject[] lightSet;
	public GameObject showShelves;
	public GameObject hideShelves;


	// Use this for initialization
	void Start () {

		lightSet = GameObject.FindGameObjectsWithTag("LightOff");

	} //end Start
	
	// Update is called once per frame
//	void Update () {
//	
//	} //end Update

	void OnTriggerEnter(Collider detectedObject){

		if (detectedObject.tag == ("Player") &&  shiftDone == false){

			//Turns Lights off
			if (lightSet.Length != 0) {
				foreach (GameObject light in lightSet){
				light.SetActive(false);
				} //end foreach
			} else {
				print ("ShiftMazeScript turns off Lights, assign 'LightOff' tag to lights");
			} //end if/else
			
			//shift inactive/active maze parts
			showShelves.SetActive(true);
			hideShelves.SetActive(false);

			shiftDone = true;
			} //end if 'player'

	} //end OnTriggerEnter

} //end class
