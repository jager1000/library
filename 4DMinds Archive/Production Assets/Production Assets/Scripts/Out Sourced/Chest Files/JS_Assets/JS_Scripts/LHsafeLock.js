/*This script handles a safe lock, which requires the activator to rotate a dial while moving a stethoscope to try and find the sound source. After that the activator
should move the dialin a direction until a loud click is heard, then he must rotate the other way until another click is heard, and so until the safe lock is open*/

#pragma strict

private var thisTransform:Transform;

//The camera of this lock
var cameraObject:Camera;

//The stethoscope object
var stethoscope:Transform;

//The hotspot object, which is the point at which you hear the loudest sound with the stethoscope
var hotspotObject:Transform;

//How quickly the stethoscope focuses on an area after we stop moving it
var focusSpeed:float = 1;
internal var focus:float = 0;

//The dial object, its rotation speed and target rotation
var dial:Transform;
var dialSpeed:float = 100;
internal var dialTarget:float = 0;

//The location of the hotspot
private var hotspot:Vector3 = Vector3.zero;

//The range of the hotspot, within this you hear the sound of the rotating dial clearly
var hotspotRange:float = 0.05;

//The falloff the hotspot, where the sound of the dial turning starts to fade until you don't hear it anymore
var hotspotFalloff:float = 0.1;

//The sequence we must follow in order to unlock this safe
var sequence:int[];
internal var sequenceIndex:int = 0;

//The direction we should rotate in ( 1 - clockwise/right, 2 - counterclockwise - left )
var direction:int = 1;

//How far in the wrong direction must a dial rotate before the whole sequence is reset 
var dialReset:float = 20;
internal var dialResetCount:float = 0;

//State of the lock: 0 intro, 1 unlocking, 2 unlocked
internal var lockState:int = 0; 

//Should the hotspot be placed randomly?
var randomHotspot:boolean = true;

//The container of this lock ( A door, a safe, etc )
internal var lockParent:Transform;

//The activator object, holds lockpicks and whatnot
internal var activator:GameObject;

//The name of the tool in the player's inventory which is required to unlock this lock ( lockpicks, bobbypins, safe cracker tools, etc )
var requiredTool:String = "safecracking tools";

//The index of the tool in the player's inventory
internal var requiredToolIndex:int;

//Various sounds
var soundTurn:AudioClip;
var soundClick:AudioClip;
var soundReset:AudioClip;
var soundUnlock:AudioClip;
var soundFail:AudioClip;

//GUI for button graphics
var GUISkin:GUISkin;

//The position and size of the "abort" button
var abortRect:Rect = Rect( 0, 0, 100, 50);
var abortText:String = "Abort";

//The description for how to play this lock
var description:String = "Move the stethoscope (Mouse) while turning the dial (A/D) until you find the spot with the highest click sound. Then turn the dial either left or right until you hear a louder click, then turn the other way until you hear another click, and so on until the sequence is complete and the safe is unlocked.";
var descriptionMobile:String = "Move the stethoscope (Swipe right side of the screen) while turning the dial (Swipe left side of the screen) until you find the spot with the highest click sound. Then turn the dial either left or right until you hear a louder click, then turn the other way until you hear another click, and so on until the sequence is complete and the safe is unlocked.";

//Holds the type of controls we use, mobile or otherwise
private var controlsType:String = "";

//touch state and index for the stethoscope
internal var touchStethoscope:boolean = false;
internal var touchStethoscopeIndex:int = -1;

//touch state and index for the dial
internal var touchDial:boolean = false;
internal var touchDialIndex:int = -1;

//Icons for the mobile controls
var mobileIcons:Transform;

//Should we lock the mouse pointer on Exit?
var lockCursorOnExit:boolean = false;

function Start() 
{
	//Detect if we are running on Android or iPhone
	#if UNITY_IPHONE
    	controlsType = "iphone";
    	print("iphone");
  	#endif
  	
  	#if UNITY_ANDROID
    	controlsType = "android";
    	print("android");
  	#endif
  	  	
  	//Caching the transform for quicker access
	thisTransform = this.transform;
  	
  	//Deactivate the activator and container objects so they don't interfere with the lockpicking gameplay
	if ( activator )    activator.SetActive(false);
  	
  	//If we are not on a mobile platform, disable the mobile icons
  	if ( controlsType == "android" || controlsType == "iphone" )
	{
		
	}
	else
	{
		if ( mobileIcons )    mobileIcons.gameObject.SetActive(false);
	}
	
	//Set a random hotspot
	if ( randomHotspot == true )
	{
		hotspotObject.localPosition.x =  GetComponent(BoxCollider).center.x + GetComponent(BoxCollider).size.x * Random.Range( -0.5, 0.5);
		hotspotObject.localPosition.y =  GetComponent(BoxCollider).center.y + GetComponent(BoxCollider).size.y * Random.Range( -0.5, 0.5);
	}
	
	//Assign the hotspot based on the object position
	hotspot.x = hotspotObject.position.x;
	hotspot.y = hotspotObject.position.y;
	hotspot.z = hotspotObject.position.z;
	
	//If we set the direction to anything other than 1 or -1, set it to 1
	if ( direction != 1 && direction != -1 )    direction = 1;
	
	//Skipping the intro animation
	lockState = 1;
	
	//Silencing the stethoscope at the start of the game
	GetComponent.<AudioSource>().volume = 0;
}

function Update() 
{
	//Show the cursor and allow it to move while we interact with the lock 
	Cursor.lockState = CursorLockMode.None;
	Cursor.visible = true;
	
	//Check touches and assign fingers to the correct controls (stethoscope or dial)
	if ( controlsType == "android" || controlsType == "iphone" )
	{
		CheckTouch();
	}
	
	//We are in the "unlocking" state
	if ( lockState == 1 )
	{
		//Change the focus based on the touch movement. The faster it moves the less focus we have
		if ( (controlsType == "android" || controlsType == "iphone") && touchStethoscopeIndex > -1 )
		{
			//If there is 1 touch on the screen, assume it belongs to this stethoscope control. If there are 2 touches, move the control based on the correct index
			if ( Input.touches.Length == 1 )
			{
				//If the touch moved, lose focus. Otherwise, return focus to normal
				if ( Input.touches[0].phase == TouchPhase.Moved )
				{
					focus = Mathf.Lerp( focus, 0, Time.deltaTime * 5);
				}
				else
				{
					focus = Mathf.Lerp( focus, 1, Time.deltaTime * 5);
				}
			}
			else if ( Input.touches.Length == 2 )
			{
				//If the touch moved, lose focus. Otherwise, return focus to normal
				if ( Input.touches[touchStethoscopeIndex].phase == TouchPhase.Moved )
				{
					focus = Mathf.Lerp( focus, 0, Time.deltaTime * 5);
				}
				else
				{
					focus = Mathf.Lerp( focus, 1, Time.deltaTime * 5);
				}
			}
		}
		else
		{
			//Change the focus based on the mouse movement. The faster it moves the less focus we have
			//If the touch moved, lose focus. Otherwise, return focus to normal
			if( Input.GetAxis("Mouse X") != 0 )
			{
				focus = Mathf.Lerp( focus, 0, Time.deltaTime * 5);
			}
			else
			{
				focus = Mathf.Lerp( focus, 1, Time.deltaTime * 5);
			}
		}
		
		//Check hits with the safe surface
		var hit:RaycastHit;
	    var ray:Ray = cameraObject.ScreenPointToRay(Input.mousePosition);
		
		//Check if we are touching the screen, moving the stethoscope
		if ( (controlsType == "android" || controlsType == "iphone") && touchStethoscopeIndex > -1 )
		{
			//If there is 1 touch on the screen, assume it belongs to this stethoscope control. If there are 2 touches, move the control based on the correct index
			if ( Input.touches.Length == 1 )
			{
				//Raycast from the camera to the touch position 
				ray = cameraObject.ScreenPointToRay(Input.touches[0].position);
			}
			else if ( Input.touches.Length == 2 )
			{
				//Raycast from the camera to the touch position 
				ray = cameraObject.ScreenPointToRay(Input.touches[touchStethoscopeIndex].position);
			}
		}
		
		//If the raycast hits
		if ( Physics.Raycast( ray, hit, 50) )
	    {
		    //Allow hits only with this safe lock
		    if ( hit.collider.transform == thisTransform )
		    {
				//Move the stethoscope along the surface of the safe
				stethoscope.position = hit.point + Vector3.forward * 0.02;
			    stethoscope.Translate( Vector3.forward * focus * -0.02, Space.Self);
			    
			    //Check the distance between the stethoscope and the hotspot
			    var distance:float = Vector3.Distance( stethoscope.position, hotspot);
			    
			    //If we are well out of the hotspot falloff, then there is no sound
			    if ( distance > hotspotRange + hotspotFalloff )
			    {
			    	GetComponent.<AudioSource>().volume = 0;
			    }
			    else if ( distance > hotspotRange )
			    {
			    	//When we start getting closer to the hotspot range, the sound starts rising
			    	GetComponent.<AudioSource>().volume = (distance - hotspotRange - hotspotFalloff)/(hotspotFalloff - hotspotRange - hotspotFalloff);
			    }
			    else
			    {
			    	//We are within the hotspot range, full sound volume
			    	GetComponent.<AudioSource>().volume = 1;
			    }
			    
			    //The sound is affected by the focus of the stethoscope. Lower focus means lower sound
			    GetComponent.<AudioSource>().volume *= focus;
			}
		}
		
		//Check if we are touching the screen, rotating the dial
		if ( (controlsType == "android" || controlsType == "iphone") && touchDialIndex > -1 )
		{
			//If there is 1 touch on the screen, assume it belongs to this dial control. If there are 2 touches, move the control based on the correct index
			if ( Input.touches.Length == 1 )
			{
				//If the touch is moving right, else if it's moving left
				if ( Input.touches[0].deltaPosition.x > 0 )
				{
					//Dial right at a normal speed
					DialRight(1);
				}
				else if ( Input.touches[0].deltaPosition.x < 0 )
				{
					//Dial left at a normal speed
					DialLeft(1);
				}
			}
			else if ( Input.touches.Length == 2 )
			{
				//If the touch is moving right, else if it's moving left
				if ( Input.touches[touchDialIndex].deltaPosition.x > 0 )
				{
					DialLeft(1);
				}
				else if ( Input.touches[touchDialIndex].deltaPosition.x < 0 )
				{
					DialRight(1);
				}
			}
		}
		else
		{
			//If we are pressing D dial left, otherwise if we press A dial right
			if ( Input.GetAxis("Horizontal") > 0 )
			{
				DialLeft(1);
			}
			else if ( Input.GetAxis("Horizontal") < 0 )
			{
				DialRight(1);
			}
		}
		
		//Allowing the reset counter to loop
		if ( dialResetCount < 0 )    dialResetCount = 0;
		if ( dialResetCount > dialReset * 3.6 )    dialResetCount = dialReset * 3.6;
		
		//If we swipe left/right, rotate the dial
		if ( (controlsType == "android" || controlsType == "iphone") && touchDialIndex > -1 )
		{
			//If there is 1 touch on the screen, assume it belongs to this dial control. If there are 2 touches, move the control based on the correct index
			if ( Input.touches.Length == 1 )
			{
				//If the touch is not stationary, run the DialRotate function
				if ( Input.touches[0].deltaPosition.x != 0 )
				{
					DialRotate();
				}
			}
			else if ( Input.touches.Length == 2 )
			{
				//If the touch is not stationary, run the DialRotate function
				if ( Input.touches[touchDialIndex].deltaPosition.x != 0 )
				{
					DialRotate();
				}
			}
		}
		else if ( Input.GetAxis("Horizontal") != 0 )
		{
			//If the mouse is moving, run the DialRotate function
			DialRotate();
		}
		else if ( lockState != 2 )
		{
			//Stop all sounds if we unlocked this safe
			if ( GetComponent.<AudioSource>().isPlaying == true )    GetComponent.<AudioSource>().Stop();
		}
		
		//If we reach the end of the sequence, unlock the safe, we WIN
		if ( sequenceIndex >= sequence.Length )    
		{
			lockState = 2; 
			
			Unlock();
		}
	}
}

//This function turns the dial counterclockwise (increasing the numbers on the dial)
function DialLeft( multiplier:float )
{
	//Set the dial target based on our turning speed
	dialTarget -= dialSpeed * Time.deltaTime * multiplier;
	
	//Reset it when it gets negative
	if ( dialTarget < 0 )    dialTarget += 360;
	
	//If we are turning the dial opposite the correct direction, it will reset the dial sequence
	if ( direction == -1 )    
	{
		DialReset();
	}
	else if ( 100 - Mathf.Round(dialTarget/3.6) == sequence[sequenceIndex] )
	{
//If we are rotating in the correct direction, go on to the next item in the sequence
		dialResetCount = 0;
		
		sequenceIndex++;
		
		direction *= -1;
		
		GetComponent.<AudioSource>().PlayOneShot(soundClick);
		
		//print( "next in sequence -> direction " + direction.ToString() + " dial " + sequence[sequenceIndex].ToString() );
	}
}

//This function turns the dial clockwise (decreasing the numbers on the dial)
function DialRight( multiplier:float )
{
	//Set the dial target based on our turning speed
	dialTarget += dialSpeed * Time.deltaTime * multiplier;
	
	//Reset it when it gets negative
	if ( dialTarget > 360 )    dialTarget -= 360;
	
	//If we are turning the dial opposite the correct direction, it will reset the dial sequence
	if ( direction == 1 )    
	{
		DialReset();
	}
	else if ( 100 - Mathf.Round(dialTarget/3.6) == sequence[sequenceIndex] )
	{
		//If we are rotating in the correct direction, go on to the next item in the sequence				
		dialResetCount = 0;
		
		sequenceIndex++;
		
		direction *= -1;
		
		GetComponent.<AudioSource>().PlayOneShot(soundClick);
	}
}

//This function plays a sound and sets the exact dial position when we rotate left or right
function DialRotate()
{
	GetComponent.<AudioSource>().clip = soundTurn;
				
	if ( GetComponent.<AudioSource>().isPlaying == false )    GetComponent.<AudioSource>().Play();
	
	dial.localEulerAngles.z = Mathf.Round(dialTarget/3.6) * 3.6;
}

//This function resets the dial sequence
function DialReset()
{
	//Count the dial reset value
	dialResetCount += dialSpeed * Time.deltaTime;

	//Reset the entire sequence if we move too much in the opposite direction
	if ( dialResetCount >= dialReset * 3.6 )
	{
		while ( sequenceIndex > 0 )
		{
			sequenceIndex--;
			
			direction *= -1;
		}
		
		dialResetCount = 0;
		
		GetComponent.<AudioSource>().PlayOneShot(soundReset);
	}
}

//This function unlocks and opens a container. After that the container will have an unlocked lock
function Unlock()
{
	//Set and play relevant sounds
	GetComponent.<AudioSource>().Stop();
	GetComponent.<AudioSource>().PlayOneShot(soundUnlock);
	
	//Wait for a second
	yield WaitForSeconds(2);
	
	//Set the container to unlocked and activate it
	if ( lockParent )
	{
		lockParent.gameObject.SetActive(true);
		lockParent.GetComponent(LHcontainer).locked = false;
		lockParent.GetComponent(LHcontainer).Activate();
	}
	
	//Exit the bomb difuse game
	Exit();
}

//This function runs when we fail to unlock the lock
function Fail()
{
	GetComponent.<AudioSource>().PlayOneShot(soundFail);
	
	//Wait for a second
	yield WaitForSeconds(1);
	
	//Activate the fail functions on the container
	if ( lockParent )
	{
		lockParent.gameObject.SetActive(true);
		lockParent.GetComponent(LHcontainer).FailActivate();
	}
	
	//Exit the bomb difuse game
	Exit();
}

//This function aborts the lockpicking gameplay and reactivates the activator
function Exit()
{
	//Activate the activator prefab, meaning that we are done with lockpicking
	activator.SetActive(true);
	
	//Enable the container script
	//lockParent.GetComponent(LHcontainer).enabled = true;
	
	//Activate the container object
	lockParent.gameObject.SetActive(true);
	
	//Lock the mouse pointer
	if ( lockCursorOnExit )    Cursor.lockState = CursorLockMode.Locked;
	
	//Destroy this lock
	Destroy(gameObject);
}

//This function check touches on mobile platforms. It looks for 2 touches, one for the stethoscope and one for the dial
function CheckTouch()
{
	//Look for 2 touches
	if ( Input.touchCount <= 2 )
	{
		//Go through all available touches
		for ( var index:int = 0 ; index < Input.touchCount ; index++ )
		{
			//If the touch just began
			if ( Input.touches[index].phase == TouchPhase.Began )
			{
				//If the touch is within the right side of the screen, assign it to the stethoscope
				if ( Input.touches[index].position.x > Screen.width * 0.6 )
				{
					touchStethoscope = true;
					touchStethoscopeIndex = index;
				}
				
				//If the touch is within the left side of the screen, assign it to the dial
				if ( Input.touches[index].position.x < Screen.width * 0.3 )
				{
					touchDial = true;
					touchDialIndex = index;
				}
			}
			
			//If a touch has ended, reset its index
			if ( Input.touches[index].phase == TouchPhase.Ended )
			{
				//Reset index for stethoscope
				if ( index == touchStethoscopeIndex )
				{
					touchStethoscope = false;
					touchStethoscopeIndex = -1;
				}
				
				//Reset index for dial
				if ( index == touchDialIndex )
				{
					touchDial = false;
					touchDialIndex = -1;
				}
			}
		}
	}
}

function OnDrawGizmos() 
{
	//The hotspot area
	Gizmos.color = Color.green;
	Gizmos.DrawSphere( Vector3( hotspot.x, hotspot.y, hotspot.z), hotspotRange);
	
	//The hotspot falloff area
	Gizmos.color = Color.red;
	Gizmos.DrawWireSphere( Vector3( hotspot.x, hotspot.y, hotspot.z), hotspotRange + hotspotFalloff);
}

function OnGUI() 
{
    GUI.skin = GUISkin;
	
    //Abort button
	if ( GUI.Button( abortRect, abortText) )
	{
		Exit();
	}
	
    //Some explanation of how to play
	if ( controlsType == "android" || controlsType == "iphone" )
	{
    	GUI.Label( Rect( 0, Screen.height - 100, Screen.width, 100), descriptionMobile);
	}
	else
	{
    	GUI.Label( Rect( 0, Screen.height - 100, Screen.width, 100), description);
	}
}