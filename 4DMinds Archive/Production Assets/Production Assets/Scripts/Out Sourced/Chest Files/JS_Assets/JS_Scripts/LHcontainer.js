/*This script handles containers, which are the objects that can be opened (door, safe, chest, etc). Each container can have a lock assigned to it, which can
be either locked or unlocked. If you click on a container with a locked lock, the lockpicking gameplay mechanism appears*/

#pragma strict

private var thisTransform:Transform;

var pauseMenu:GameObject;

var disable:Collider;
//The lock object that will be activated when clicking on this container
var lockobj:Transform;

//Is the container locked?
var locked:boolean = true;

//A GUI for buttons and labels
var guiSkin:GUISkin;

var lockedIcon:Texture2D; // Display icon when the lock is locked.
var unlockedIcon:Texture2D; // Display icon when the lock is unlocked.

//Text to  display when the lock is locked/unlocked
var lockedCaption:String = "Locked Door";
var unlockedCaption:String = "Open Door";

//GUI size for the captions
var captionWidth:float = 300;
var captionHeight:float = 100;

//Various sounds for the container
var soundLocked:AudioClip;
var soundActivate:AudioClip;

//The tag of the activator. This means that this container can be activated by the object with this tag only
var activatorTag:String = "Player";

//The button that activates this container
var activateButton:String = "Fire1";

//The offset from the activator when creating the lock
var lockOffset:Vector3 = Vector3(0,0,0);

//The activator object that activates the container
internal var activator:GameObject;

//Is the activator detected
internal var activatorDetected:boolean = false;

static var picksLeft:int = 20;

//A list of functions to be activated. Consists of a reciever and the name of the function to be run in it
var activateFunctions:ActivateFunctions[];

class ActivateFunctions 
{
    var reciever:Transform;
    var functionName:String;
}

//A list of animations that are played in sequence activating this container. If no animation is set, the default animation is played
var activateAnimations:AnimationClip[];
var animationIndex:int = 0;

//A list of functions to be activated on failure. Consists of a reciever and the name of the function to be run in it
var failFunctions:FailFunctions[];

class FailFunctions 
{
    var reciever:Transform;
    var functionName:String;
}

//A general use index
internal var index:int = 0;

//The name of the tool in the player's inventory which is required to unlock this lock ( lockpicks, bobbypins, safe cracker tools, etc )
internal var requiredTool:String = "";

//The index of the tool in the inventory
internal var requiredToolIndex:int;

//Holds the type of controls we use, mobile or otherwise
private var controlsType:String = "";

//Time since we started the touch
private var timeSinceTap:float = 0;

function Start()
{
	//Detect if we are running on Android or iPhone
	#if UNITY_IPHONE
    	controlsType = "iphone";
    	print("iphone");
  	#endif
  	
  	#if UNITY_ANDROID
    	controlsType = "android";
    	print("android");
  	#endif
	
	//Caching the transform object for quicker access
	thisTransform = transform;
	
	//Assign the activator object by its tag
	activator = GameObject.FindGameObjectWithTag(activatorTag);
	
	//If there is a lock object assigned
	if ( lockobj )
	{
		//Get the name of the required tool from the locks and assign them to requiredTool
		if ( lockobj.GetComponent(LHlock) )
		{
			requiredTool = lockobj.GetComponent(LHlock).requiredTool;
		}
		else if ( lockobj.GetComponent(LHsafeLock) )
		{
			requiredTool = lockobj.GetComponent(LHsafeLock).requiredTool;
		}
		else if ( lockobj.GetComponent(LHbombLock) )
		{
			requiredTool = lockobj.GetComponent(LHbombLock).requiredTool;
		}
		else if ( lockobj.GetComponent(LHpanelLock) )
		{
			requiredTool = lockobj.GetComponent(LHpanelLock).requiredTool;
		}
	}
}

function Update()
{
	//If an activator has been detected, wait for a button/touch
	if ( activatorDetected )
	{
		//Check touch
		if ( controlsType == "android" || controlsType == "iphone" )
		{
			//If there is more than 1 touch
			if( Input.touchCount > 0 )
			{
				//If the touch began, record its time
				if ( Input.GetTouch(0).phase == TouchPhase.Began )
				{
					timeSinceTap = Time.time;
				}
				
				//If the touch ended, calculate its end time
				if ( Input.GetTouch(0).phase == TouchPhase.Ended )
				{
					//If the touch time is less than 0.3 seconds, Activate!
					if ( Time.time - timeSinceTap < 0.3 )
					{
						Activate();
					}
				}
			}
		}
		else if ( activateButton )
		{
			//If we press the activate button, Activate!
			if ( Input.GetButtonDown(activateButton) )
			{
				Activate();
			}
		}
		else
		{
			//If no activate button is assigned, activate automatically
			Activate();
		}
	}
}

function OnTriggerEnter(other:Collider) 
{
	if ( other.tag == activator.tag )
	{
		activatorDetected = true;
	}	
}

function OnTriggerExit(other:Collider) 
{
	if ( other.tag == activator.tag )
	{
		activatorDetected = false;
	}	
}

//A simple function to override the triggers and run the activator on demand
function ManualActivate()
{
	activatorDetected = true;

}

//This function activates a container: Checks if we have a lock, if it's locked, and if we have lockpicks
function Activate()
{
	//If the container is locked, check if we have a lock assigned to it, in which case the lockpicking gameplay mechanism appears
	if ( locked == true )
	{
		print("activating lock");
		//Create a lockpick object and place it in front of the camera, activate lock
		if ( lockobj )
		{
			activatorDetected = false;
			
			//If the activator is currently active...
			if ( activator.activeSelf || !GetComponent.<Collider>() )
			{
				//Find the activator component and check if he has any picks left
				if ( CheckRequiredTool() || requiredTool == "" )
				{
					//Create a new lock object and place at the center of the screen
					var newLock:Transform = Instantiate( lockobj, activator.transform.position + lockOffset, Quaternion.identity);
					
					//Check the type of lock component and assign it to the activator accordingly
					if ( newLock.GetComponent(LHlock) )
					{
						newLock.GetComponent(LHlock).lockParent = thisTransform;
						newLock.GetComponent(LHlock).activator = activator;
					}
					else if ( newLock.GetComponent(LHsafeLock) )
					{
						newLock.GetComponent(LHsafeLock).lockParent = thisTransform;
						newLock.GetComponent(LHsafeLock).activator = activator;
					}
					else if ( newLock.GetComponent(LHbombLock) )
					{
						newLock.GetComponent(LHbombLock).lockParent = thisTransform;
						newLock.GetComponent(LHbombLock).activator = activator;
					}
					else if ( newLock.GetComponent(LHpanelLock) )
					{
						newLock.GetComponent(LHpanelLock).lockParent = thisTransform;
						newLock.GetComponent(LHpanelLock).activator = activator;
					}
					
					//Deactivate the activator script so it doesn't interfere with the lockpicking gameplay
					activator.SetActive(false);
	
					//Disable this script while we interact with the lock
					//this.enabled = false;
					
					//Deactivate this gameObject while we interact with the lock
					gameObject.SetActive(false);
				}
				else
				{
					print("You don't have any " + requiredTool);
					
					activatorDetected = true;
					
					GetComponent.<AudioSource>().PlayOneShot(soundLocked);
				}
			}
		}
		else
		{
			//The door can't be opened. Use this to make doors that can't be opened with lockpicking
			print("This container can't be unlocked");
			
			GetComponent.<AudioSource>().PlayOneShot(soundLocked);
		}
	}
	else
	{
		//If the container is not locked, activate it
		for ( var index in activateFunctions )
		{
			if ( index.reciever && index.functionName )    index.reciever.SendMessage(index.functionName);
		}
	}
	if(locked == false){
		disable.enabled = false;
		disable.isTrigger = false;
		this.enabled = false;
	}
}

//This function activates when you fail at unlocking a lock. It's used for lock such as the bombLock
function FailActivate()
{
	//Activate all the fail functions
	for ( var index in failFunctions )
	{
		if ( index.reciever && index.functionName )    index.reciever.SendMessage(index.functionName);
	}
}

//A function that animates this container and plays a sound
function AnimateContainer()
{
	if ( GetComponent.<Animation>() )
	{
		if ( !GetComponent.<Animation>().isPlaying )
		{
			// If an activateAnimations is set, play it. Otherwise play the default animation
			if ( activateAnimations.Length > 0 )
			{
				if ( activateAnimations[animationIndex] )
				{
					//Play the current activation animation
					GetComponent.<Animation>().Play(activateAnimations[animationIndex].name);
					
					//Cycle through the list of animations
					if ( animationIndex < activateAnimations.Length - 1 )    animationIndex++;
					else    animationIndex = 0;
				}
			}
			else
			{
				GetComponent.<Animation>().Play();
			}
			
			//Play a sound if avaialble
			if ( soundActivate)    GetComponent.<AudioSource>().PlayOneShot(soundActivate);
		}
	}
}

//This funtion goes through all the items in the activator's inventory and checks if we have at least 1 of the tool required to unlock this container
function CheckRequiredTool():boolean
{
	var returnValue:boolean = false;
	
	if ( requiredTool != "" )
	{
		if ( lockobj )
		{
			//Go through all tools in the inventory
			for ( var index = 0 ; index < activator.GetComponent(LHinventory).inventory.length ; index++ )
			{
				//If we have the correct tool name, and we have at least 1 of it, return true
				if ( activator.GetComponent(LHinventory).inventory[index].tool == requiredTool && activator.GetComponent(LHinventory).inventory[index].count > 0 )
				{
					returnValue = true;
					
					//Assign the index of the tool from the inventory to the lock object
					if ( lockobj.GetComponent(LHlock) )
					{
						lockobj.GetComponent(LHlock).requiredToolIndex = index;
					}
					else if ( lockobj.GetComponent(LHsafeLock) )
					{
						lockobj.GetComponent(LHsafeLock).requiredToolIndex = index;
					}
					else if ( lockobj.GetComponent(LHbombLock) )
					{
						lockobj.GetComponent(LHbombLock).requiredToolIndex = index;
					}
					else if ( lockobj.GetComponent(LHpanelLock) )
					{
						lockobj.GetComponent(LHpanelLock).requiredToolIndex = index;
					}
				}
			}
		}
	}
	else
	{
		returnValue = true;
	}
	
	return returnValue;
}

function OnGUI()
{
	GUI.skin = guiSkin;
	
	if ( activatorDetected == true )
	{
		if ( locked == true )
		{
			//An icon that displays when the container is locked
			if ( lockedIcon )    GUI.DrawTexture(new Rect(Screen.width * 0.5f - lockedIcon.width * 0.5f, Screen.height * 0.5f - lockedIcon.height * 0.5f, lockedIcon.width, lockedIcon.height), lockedIcon);
			
			//A caption that displays when the container is locked
			if ( lockedCaption != "" )    GUI.Label(Rect ( Screen.width * 0.5 - captionWidth * 0.5, Screen.height * 0.5 - captionHeight * 0.5, captionWidth, captionHeight), lockedCaption);
			
			//if ( requiredTool != "" )
			//{
				if ( CheckRequiredTool() || requiredTool == "" )
				{
					//A caption that is displayed on a lock that we can open, but only if we have enough lockpicks
					//if ( activateButton && lockobj )    GUI.Label(Rect ( Screen.width * 0.5 - captionWidth * 0.5, Screen.height * 0.5 + captionHeight * 0.5, captionWidth, captionHeight), "Press " + activateButton + " to unlock");
				}
				else
				{
					//A caption that is displayed on a lock that we can open, but we don't have any lockpicks left
					if ( activateButton && lockobj )    GUI.Label(Rect ( Screen.width * 0.5 - captionWidth * 0.5, Screen.height * 0.5 + captionHeight * 0.5, captionWidth, captionHeight), "You don't have " + requiredTool);
				}
			//}
		}
		else
		{
			//An icon that displays when the container is unlocked
			if ( unlockedIcon )    GUI.DrawTexture(new Rect(Screen.width * 0.5f - lockedIcon.width * 0.5f, Screen.height * 0.5f - lockedIcon.height * 0.5f, lockedIcon.width, lockedIcon.height), unlockedIcon);
			
			//A caption that displays when the container is unlocked
			if ( unlockedCaption != "" )    GUI.Label(Rect ( Screen.width * 0.5 - captionWidth * 0.5, Screen.height * 0.5 - captionHeight * 0.5, captionWidth, captionHeight), unlockedCaption);
			
			//A caption that is displayed on a lock that is unlocked
			//if ( activateButton && lockobj )    GUI.Label(Rect ( Screen.width * 0.5 - captionWidth * 0.5, Screen.height * 0.5 + captionHeight * 0.5, captionWidth, captionHeight), "Press " + activateButton + " to open");
		}
	}
}
