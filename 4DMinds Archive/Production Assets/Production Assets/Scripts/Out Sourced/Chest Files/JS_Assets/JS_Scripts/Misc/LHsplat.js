﻿//This scipt displays a splat, and fades it out as it slides down
#pragma strict

private var thisTransform:Transform;

//How quickly the splat fades out
var fadeSpeed:float = -1;

//The speed at which the splat spreads
var splat:float = 1.2;

//The movement speed of the splat
var moveSpeed:Vector3;

function Start() 
{
	thisTransform = transform;
}

function Update() 
{
	//Scale the splat object up to give a splat effect
	if ( Mathf.Abs(splat) > 1.01 )
	{
		//Scale the splat to give an effect of smacking against the camera
		thisTransform.localScale *= splat;
		splat -= (splat - 1) * 0.5;
	}
	else
	{
		//Fade out the splat object
		GetComponent.<Renderer>().material.color.a += fadeSpeed * Time.deltaTime;
		
		thisTransform.position += moveSpeed * Time.deltaTime;
	}
}