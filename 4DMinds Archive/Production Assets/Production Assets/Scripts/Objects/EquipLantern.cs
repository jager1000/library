﻿using UnityEngine;
using System.Collections;

public class EquipLantern : MonoBehaviour {

    public float holdTime = 0;
    public bool lanternUp = true;
    public Transform lanternPos;
    public GameObject lantern;
	public float countdownTimer;
	public float almostOutTime;
	public float lanternOut = 0.0f;
	public Light lanternLight;
	public bool hasFuel;

	void Start(){
		hasFuel = true;
		startTimer ();
	}

	void Update () {
		almostOut ();
            if(Input.GetMouseButtonUp(1)){
                holdTime += Time.deltaTime;
                if(lanternUp == false){
                    lanternUp = true;
                lantern.SetActive(true);
                    holdTime = 0;
                }else
            if(lanternUp == true){
                    lanternUp = false;
                    lantern.SetActive(false);
                    holdTime = 0;
            }
		}
        if(Input.GetKeyDown(KeyCode.LeftControl)){
            lantern.SetActive(false);
        }
        if(Input.GetKeyUp(KeyCode.LeftControl)){
            lantern.SetActive(true);
        }
    }

	public void startTimer(){
        if (lanternUp == true && hasFuel == true){    
            countdownTimer -= Time.deltaTime; 
            Debug.Log(countdownTimer);
        }
	}

	public void almostOut(){
		lanternLight = lantern.GetComponent<Light>();

		if (countdownTimer <= almostOutTime) {
			lanternLight.intensity = 0.5f;
		}

		if (countdownTimer == lanternOut) {
			lanternLight.intensity = 0.0f;
			hasFuel = false;
		}

		if (countdownTimer >= lanternOut){
			hasFuel = true;
		}
	}
}
