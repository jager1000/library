﻿using System;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections;

public class LerpAtoB : MonoBehaviour {
    public Transform startMarker;
    public Transform endMarker;
    public float speed = 1.0F;
    private float startTime;
    private float journeyLength;
    private bool endPoint;
    private bool fpsScript;
    private bool cont;

    void Start() {
        startTime = Time.time;
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
        cont = GetComponent<CharacterController>().enabled;
        fpsScript = GetComponent<FirstPersonController>().enabled;
    }
    void Update() {
        if(this.transform.position != endMarker.transform.position){
            cont = false;
            fpsScript = false;
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        transform.position = Vector3.Lerp(startMarker.position, endMarker.position, fracJourney);
        }else{
           // cont = true;
           // fpsScript = true;
        }
    }
}