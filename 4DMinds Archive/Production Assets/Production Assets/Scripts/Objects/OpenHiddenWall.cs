﻿using UnityEngine;
using System.Collections;

public class OpenHiddenWall : MonoBehaviour {
    
    // Smothly open a door
    public float smooth = 2.0f;
    public float DoorOpenAngle = 90.0f;
    private bool open = false;
    private bool enter = false;
    private Vector3 defaultRot;
    private Vector3 openRot;
    private AudioSource source;
    private SoundPlaceHolder soundHolder;
    
    // Use this for initialization
    void Start () {
        defaultRot = transform.eulerAngles;
        openRot = new Vector3 (defaultRot.x, defaultRot.y + DoorOpenAngle, defaultRot.z);
        source = GetComponent<AudioSource>();
        soundHolder = GameObject.FindGameObjectWithTag("SoundHolder").GetComponent<SoundPlaceHolder>();
        
    }
    
    void Update () {
        if(open){
            //Open door
            
            transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, openRot, Time.deltaTime * smooth);
        }else{
            //Close door
            transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, defaultRot, Time.deltaTime * smooth);
        }
        
        if(Input.GetKeyDown("f") && enter){
            open = !open;
            if(open == true){
                source.PlayOneShot(soundHolder.OpenSecretDoor, 1.0f);
            }
            if(open == false){
                source.PlayOneShot(soundHolder.Close[2], 1.0f);
            }
        }
    }
    
    void OnGUI(){
        if(enter){
            GUI.Label(new Rect(Screen.width/2 - 75, Screen.height - 100, 150, 30), "Press 'F' to open");
        }
    }
    
    //Activate the Main function when player is near the door
    void OnTriggerEnter (Collider other){
        if (other.gameObject.tag == "Player") {
            enter = true;
        }
    }
    
    //Deactivate the Main function when player is go away from door
    void OnTriggerExit (Collider other){
        if (other.gameObject.tag == "Player") {
            enter = false;
        }
    }
}