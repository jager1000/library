﻿using UnityEngine;
using System.Collections;

public class WindowSlam : MonoBehaviour {

	public int index = -1;
	public bool open = true;
	public float windowOpenAngle = 90f;
	public float windowCloseAngle = 0f;
	public float smooth = 2f;
	
	private AudioSource source;
	
	void Start(){
		source = GetComponent<AudioSource>();
	}
	
	void Update () {
		if(open){
			Quaternion targetRotation = Quaternion.Euler(0,windowOpenAngle,0);
			transform.localRotation = Quaternion.Slerp(transform.localRotation,targetRotation,smooth * Time.deltaTime);
			source.Play();
		}else{
			Quaternion targetRotation2 = Quaternion.Euler(0,windowCloseAngle,0);
			transform.localRotation = Quaternion.Slerp(transform.localRotation,targetRotation2,smooth * Time.deltaTime);
		}
	}
	
	public void CloseWindow(){
		open = !open;
	}
}