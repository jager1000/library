﻿using UnityEngine;
using System.Collections;

public class ClockScareEvent : MonoBehaviour {

    public AudioClip scareSound;
    private bool oneShot = false;

    void OnTriggerEnter(Collider other){
        if(other.CompareTag("Player") && oneShot == false){
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().Play();
            oneShot = true;
        }
    }

}
