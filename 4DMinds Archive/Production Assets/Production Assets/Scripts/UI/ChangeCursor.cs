﻿using UnityEngine; 
using System.Collections; 

public class ChangeCursor : MonoBehaviour{ 
	public Texture2D[] cursorTexture; 
	public bool ccEnabled = false; 
	private Vector3 mousePos;
	
	void Start(){   
		mousePos = Input.mousePosition;
		mousePos.z = 5.0f;
        SetCustomCursor(2);
	} 

	void Update(){
		Vector3 p = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2, Screen.height/2, Camera.main.nearClipPlane + 1)); 
		RaycastHit hit = new RaycastHit ();
		if (Physics.Raycast (p, Camera.main.transform.forward, out hit, 1)) {

            if(hit.collider.tag == "Lantern" || hit.collider.tag == "Collectable" || hit.collider.tag == "Key" || hit.collider.tag == "Note") {
				SetCustomCursor(0);
			}
//			if(hit.collider.tag == "Collectable"){
//				SetCustomCursor(1);
//			}
        } else{
            SetCustomCursor(2);
        }
	}
	
	void OnDisable(){ 
		Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);  
		this.ccEnabled = false; 
	} 
	
	private void SetCustomCursor(int set){   
		Cursor.SetCursor(this.cursorTexture[set], Vector2.zero, CursorMode.Auto); 
		this.ccEnabled = true; 
	} 
} 
