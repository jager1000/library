﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeOutScript : MonoBehaviour {
    
    public Image image;
    public GameObject fadeOutImage;
    public float fadeIn = 30f;
    
    void Start () {
        image.enabled = true;
    }
    
    // Update is called once per frame
    void Update () {
        Color color = GetComponent<CanvasRenderer>().GetColor();
        color.a += fadeIn * Time.deltaTime; 
        GetComponent<CanvasRenderer>().SetColor(color);
        if(color.a >= 0.0f){
            fadeOutImage.SetActive(true);
        }
        if(color.a >= 255f){
            Cursor.lockState = CursorLockMode.None;
            Application.LoadLevel(0);
        }
    }
}
