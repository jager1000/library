﻿using UnityEngine;
using System.Collections;

public class MouseLock : MonoBehaviour {
	private bool locked;
	
	void Start() {
        if(!Debug.isDebugBuild){
            locked = true;
            Cursor.lockState = CursorLockMode.Locked;
        }
	}
	
	void Update () {
        if(Debug.isDebugBuild){
            if (Input.GetButtonUp("MouseLock") && !locked) {
                Lock();
            }
            
            if (Input.GetButtonUp("MouseUnlock") && !locked) {
                Unlock();
            }
        }
	}
	
	void Lock() {
		Cursor.lockState = CursorLockMode.Locked;
	}
	
	void Unlock() {
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}
}