﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Audio;

public class FadeInScript : MonoBehaviour {

    public Image image;
    public GameObject fadeInImage;
    public float fadeIn = 0.05f;

	public AudioMixerSnapshot outside;

	void Start () {
        image.enabled = true;
		outside.TransitionTo (4f);
	}
	
	// Update is called once per frame
	void Update () {
        Color color = GetComponent<CanvasRenderer>().GetColor();
        color.a -= fadeIn * Time.deltaTime;
        
        GetComponent<CanvasRenderer>().SetColor(color);
        if(color.a <= 0.0f){
            fadeInImage.SetActive(false);
            //image.enabled = false;
        }
	}
}
