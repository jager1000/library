﻿using UnityEngine;
using System.Collections;

public class OpenDoorLibrarian : MonoBehaviour {

	public DoorScript door;

	void Start(){
		if (!door.open) {
			door.ChangeDoorState ();
		}
	}
}
