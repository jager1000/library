﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class CreakyFloor : MonoBehaviour {
	
	public AudioSource source;
	public AudioClip[] creaks;

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player"))
		{
			int n = Random.Range (0,creaks.Length);
			source.PlayOneShot(creaks[n],0.6f);
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.CompareTag("Player"))
		{
			int n = Random.Range (0,creaks.Length);
			source.PlayOneShot(creaks[n],0.6f);
		}
	}
}
