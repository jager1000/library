﻿using UnityEngine;
using System.Collections;

public class OnTopOfShed : MonoBehaviour {

	private bool triggered = false;
	private bool played = false;
	private AudioSource source;

	void Start(){
		source = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Player")) {
			triggered = true;
		}
	}

	// Update is called once per frame
	void Update () {
		
		if (triggered) {
			transform.Translate (Vector3.forward * Time.deltaTime * 2.25f);
			if (!played){
				source.Play();
				played = true;
			}
			Destroy (gameObject, 7f);
		}
	}


}

