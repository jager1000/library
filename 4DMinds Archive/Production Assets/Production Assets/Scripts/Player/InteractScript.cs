﻿using UnityEngine;
using System.Collections;

public class InteractScript : MonoBehaviour {

    public float interactDistance = 5f;
    public GameObject book;
    public GameObject lantern;
    public GameObject logs;
	public GameObject fuel;

	public AudioClip pickUpPage;
	public AudioClip pickUpKey;
	public AudioClip pickUpOther;

    public GameObject doorLock;

    public GameObject Librarian;
    public GameObject hiddenWall;
    private AudioSource source;
    private SoundPlaceHolder soundHolder;

    void Start(){
        source = GetComponent<AudioSource>();
        soundHolder = GameObject.FindGameObjectWithTag("SoundHolder").GetComponent<SoundPlaceHolder>();
        this.GetComponent<EquipLantern>().enabled = false;
        this.GetComponent<KillPlayer>().enabled = false;
        lantern.SetActive(false);
        logs.SetActive(false);
        book.SetActive(false);
        Librarian.SetActive(false);
    }

	void Update(){
        if (Input.GetKeyDown(KeyCode.Mouse0)){
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, interactDistance)){
                if (hit.collider.CompareTag("Door")){
                    DoorScript doorScript = hit.collider.transform.parent.GetComponent<DoorScript>();
					if (doorScript.index == 99){
						doorScript.PlayLockedNoise();
						return;
					}
                    if (Inventory.keys [doorScript.index] == true){
                        doorScript.ChangeDoorState();
                    } 
					if (Inventory.keys [doorScript.index] == false){
						doorScript.PlayLockedNoise();
                	}
				}
                else if(hit.collider.CompareTag("Note")){
                    Inventory.notes [hit.collider.GetComponent<Notes>().index] = true;
                    Destroy(hit.collider.gameObject);
					source.PlayOneShot(pickUpPage);
                }
                else if(hit.collider.CompareTag("Fireplace") && Inventory.objectives[0] == true){
                    logs.SetActive(true);
					if(hiddenWall.GetComponent<DoorScript>().open == false){
						hiddenWall.GetComponent<DoorScript>().PlaySecretDoorNoise();
					}
                    doorLock.GetComponent<DoorScript>().open = false;
                    doorLock.GetComponent<DoorScript>().index = 99;
                    hiddenWall.GetComponent<DoorScript>().open = true;
                }
                else if(hit.collider.CompareTag("Bundle") && Inventory.objectives[1] == true){
                    book.SetActive(true);
                }
                else if(hit.collider.CompareTag("Lantern")){
                    this.GetComponent<EquipLantern>().enabled = true;
                    lantern.SetActive(true);
                    Destroy(hit.collider.gameObject);
                }
                else if(hit.collider.CompareTag("Collectable")){
                    Inventory.objectives [hit.collider.GetComponent<Collectable>().index] = true;
                    Destroy(hit.collider.gameObject); 
					source.PlayOneShot(pickUpOther);
                }
                else if(hit.collider.CompareTag("Key")){
                    source.PlayOneShot(soundHolder.PickupKey, 0.75f);
                    Inventory.keys [hit.collider.GetComponent<Key>().index] = true;
                    Destroy(hit.collider.gameObject);
					source.PlayOneShot(pickUpKey);
                }

				else if(hit.collider.CompareTag("Fuel")){
					source.PlayOneShot(pickUpOther);
					EquipLantern changeTimer = this.GetComponent<EquipLantern>();
					changeTimer.countdownTimer += 600.0f;
					Destroy(hit.collider.gameObject);
				}

                if(Inventory.objectives[1] == true){
                    Librarian.SetActive(true);
                }else{
                    Librarian.SetActive(false);
                }

                if(book.activeInHierarchy){
                    this.GetComponent<KillPlayer>().enabled = true;
                }
            }
        }
    }


}
