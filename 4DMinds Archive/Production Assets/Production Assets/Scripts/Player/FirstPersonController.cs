using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson{
	[RequireComponent(typeof(CharacterController))]
	[RequireComponent(typeof(AudioSource))]
	public class FirstPersonController : MonoBehaviour	{
		[SerializeField]
		private bool m_IsWalking;
		[SerializeField]
		private float m_WalkSpeed = 0;
		[SerializeField]
		private float m_RunSpeed = 0;
		[SerializeField]
		[Range(0f, 1f)]
		private float m_RunstepLenghten = 0.0f;
		[SerializeField]
		private float m_JumpSpeed = 0.0f;
		[SerializeField]
		private float m_StickToGroundForce = 0.0f;
		[SerializeField]
		private float m_GravityMultiplier = 0.0f;
		[SerializeField]
		private MouseLook m_MouseLook = null;
		[SerializeField]
		private bool m_UseFovKick = true;
		[SerializeField]
		private FOVKick m_FovKick = new FOVKick ();
		[SerializeField]
		private bool m_UseHeadBob = true;
		[SerializeField]
		private CurveControlledBob m_HeadBob = new CurveControlledBob ();
		[SerializeField]
		private LerpControlledBob m_JumpBob = new LerpControlledBob ();
		[SerializeField]
		private float m_StepInterval = 0.0f;
		[SerializeField]

		private AudioClip[] m_StoneStepsWalk = null;    // an array of footstep sounds that will be randomly selected from.
		[SerializeField]
        private AudioClip[] m_StoneStepsRun = null;
		[SerializeField]
        private AudioClip[] m_StoneStepsSneak = null; 
		[SerializeField]
        private AudioClip[] m_WoodStepsWalk = null;
		[SerializeField]
        private AudioClip[] m_WoodStepsRun = null;
		[SerializeField]
        private AudioClip[] m_WoodStepsSneak = null;
		[SerializeField]
        private AudioClip[] m_CarpetStepsWalk = null;
		[SerializeField]
        private AudioClip[] m_CarpetStepsRun = null;
		[SerializeField]
		private AudioClip[] m_CarpetStepsSneak = null;

		[SerializeField]
		private AudioClip[] m_GrassStepsWalk = null;
		[SerializeField]
		private AudioClip[] m_GrassStepsRun = null;
		[SerializeField]
		private AudioClip[] m_GrassStepsSneak = null;

		[SerializeField]
		private AudioClip m_JumpSound = null;           // the sound played when character leaves the ground.
		[SerializeField]
		private AudioClip m_LandSound = null;           // the sound played when character touches back on ground.
		

		private Camera m_Camera;
		private bool m_Jump;
		private float m_YRotation;
		private Vector3 m_Input;
		private Vector3 m_MoveDir = Vector3.zero;
		private CharacterController m_CharacterController;
		private CollisionFlags m_CollisionFlags;
		private bool m_PreviouslyGrounded;
		private Vector3 m_OriginalCameraPosition;
		private float m_StepCycle;
		private float m_NextStep;
		private bool m_Jumping;
		private AudioSource m_AudioSource;
		public float jumpVolume = 0.1f;
		public Light lanternLight;
		public ParticleSystem lanternPartical;
		public GameObject lanternObject;

        private float spawnCap = 0;
        private float forwardSpeed = 0.0f;
        private Transform currentPos;
		private Transform bookLandPos;
		private Vector3 MCF;
        private AudioSource source;
        private SoundPlaceHolder soundHolder;
															// -nikki code
		public string playerMovementState = "isStill";		// controls the spectre's detection radius
		private float playerCurrentSpeed = 0;				// for stillness check
		private Vector3 previousPos;						// for calculating speed
		private int zeroCount = 0;							// to make sure player is still
		private bool running = false;						// to make check if player is running
		private bool jumping = false;						// to make check if player is running
		private bool crouching = false;						// to make check if player is running
		private bool holdingBreath = false;					// to make check if player is running


		// Use this for initialization
		private void Start ()		{
			m_CharacterController = GetComponent<CharacterController> ();
			m_Camera = Camera.main;
			m_OriginalCameraPosition = m_Camera.transform.localPosition;
			m_FovKick.Setup (m_Camera);
			m_HeadBob.Setup (m_Camera, m_StepInterval);
			m_StepCycle = 0f;
			m_NextStep = m_StepCycle / 2f;
			m_Jumping = false;
			m_AudioSource = GetComponent<AudioSource> ();
			m_MouseLook.Init (transform, m_Camera.transform);
			spawnCap = Time.time + 1;
            source = GetComponent<AudioSource>();
//            soundHolder = GameObject.FindGameObjectWithTag("SoundHolder").GetComponent<SoundPlaceHolder>();
			//!! soundholder is commented out cause it was causing an error!!
		}

		// Update is called once per frame
		private void Update (){

			// - nikki code 
			// calculate the speed of the player
			if (previousPos != transform.position) {
				playerCurrentSpeed = ((transform.position - previousPos).magnitude) / Time.deltaTime;
				previousPos = transform.position;
				zeroCount = 0;
			} else {
				playerCurrentSpeed = 0;
				if (zeroCount < 1000){		//prevent overflow
					zeroCount = zeroCount + 1;
				} else {
					zeroCount = 5;
				}
			}
			// - nikki code
			// this code adjusts the spectre's player detection radius
			if (zeroCount > 2) {
				playerMovementState = "isStill";
			} else if (holdingBreath == true) {
				playerMovementState = "isHoldingBreath";
			} else if (jumping == true) {
				playerMovementState = "isJumping";
			} else if (m_IsWalking == false) {
				playerMovementState = "isRunning";
			} else if (crouching == true) {
				playerMovementState = "isCrouching";
			} else {
				playerMovementState = "isWalking";
			}							//-


			currentPos = this.transform;
			forwardSpeed = Vector3.Dot(GetComponent<Rigidbody>().velocity, Vector3.forward);
			RotateView ();
			// the jump state needs to read here to make sure it is not missed
			if (!m_Jump) {
				m_Jump = CrossPlatformInputManager.GetButtonDown ("Jump");
			}

			if (!m_PreviouslyGrounded && m_CharacterController.isGrounded) {
				StartCoroutine (m_JumpBob.DoBobCycle ());
				PlayLandingSound ();
				m_MoveDir.y = 0f;
				m_Jumping = false;
				jumping = false;		//-

			}
			if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded) {
				m_MoveDir.y = 0f;
			}

			m_PreviouslyGrounded = m_CharacterController.isGrounded;


			//kyle code Controls crouching - needs changing(move camera.pos.y and player.height) not scale
			if (Input.GetKeyDown(KeyCode.LeftControl)){
                transform.localScale = new Vector3 (1,0.5f,1);
				m_WalkSpeed = m_WalkSpeed/2.0f;
				lanternLight.enabled = false;
				lanternPartical.enableEmission = false;
				//lanternObject.SetActive(false);
				crouching = true;		//-

			}
			if (Input.GetKeyUp(KeyCode.LeftControl)) {
                transform.localScale = new Vector3 (1, 1, 1);
				m_WalkSpeed = m_WalkSpeed * 2.0F;
				lanternLight.enabled = true;
				lanternPartical.enableEmission = true;
				//lanternObject.SetActive(true);
				crouching = false;		//-
			}

		}

		private void PlayLandingSound (){
			m_AudioSource.PlayOneShot(m_LandSound, jumpVolume);
			m_NextStep = m_StepCycle + 1f;
		}

		private void FixedUpdate (){
			float speed;
			GetInput (out speed);
			// always move along the camera forward as it is the direction that it being aimed at
			Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

			// get a normal for the surface that is being touched to move along it
			RaycastHit hitInfo;
			Physics.SphereCast (transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                               m_CharacterController.height / 2f);
			desiredMove = Vector3.ProjectOnPlane (desiredMove, hitInfo.normal).normalized;

			m_MoveDir.x = desiredMove.x * speed;
			m_MoveDir.z = desiredMove.z * speed;


			if (m_CharacterController.isGrounded) {
				m_MoveDir.y = -m_StickToGroundForce;

				if (m_Jump) {
					jumping = true;
					m_MoveDir.y = m_JumpSpeed;
					PlayJumpSound ();
					m_Jump = false;
					m_Jumping = true;
				}
			} else {
				m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
			}
			m_CollisionFlags = m_CharacterController.Move (m_MoveDir * Time.fixedDeltaTime);

			ProgressStepCycle (speed);
			UpdateCameraPosition (speed);
		}

		private void PlayJumpSound (){
			m_AudioSource.PlayOneShot(m_JumpSound, jumpVolume);
		}

		private void ProgressStepCycle (float speed){
			if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0)) {
				m_StepCycle += (m_CharacterController.velocity.magnitude + (speed * (m_IsWalking ? 1f : m_RunstepLenghten))) *
					Time.fixedDeltaTime;
			//	running = true;
			}

			if (!(m_StepCycle > m_NextStep)) {
			//	running = false;
				return;
			}

			m_NextStep = m_StepCycle + m_StepInterval + 0.25f;

			DecipherFootStepAudio ();
		}

		private void PlayFootStepAudio(AudioClip[] footstep, float volume){
			
			// pick & play a random footstep sound from the array,
			// excluding sound at index 0
			int n = Random.Range (1, footstep.Length);
			m_AudioSource.clip = footstep [n];
			m_AudioSource.pitch = Random.Range(0.8f, 1.25f);
			m_AudioSource.PlayOneShot (m_AudioSource.clip, volume);
			// move picked sound to index 0 so it's not picked next time
			footstep [n] = footstep [0];
			footstep [0] = m_AudioSource.clip;
		}

		/// <summary>
		/// Deciphers which footstep audio to play, and calls PlayFootStepAudio to play it.
		/// 
		/// Pre: things the player walks on require a label of either "WoodenFloor", "StoneFloor" or "Carpet"
		/// Post: appropriate footstep is played
		/// </summary>
		private void DecipherFootStepAudio (){
			if (!m_CharacterController.isGrounded) {
				return;
			}

			RaycastHit hit;
			Physics.Raycast(transform.position, Vector3.down, out hit, 1.8f);


			if (hit.collider.CompareTag ("WoodenFloor") && Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_WoodStepsRun, 0.4f);
			}
			else if (hit.collider.CompareTag ("WoodenFloor") && !Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_WoodStepsWalk, 0.4f);
			}
			else if (hit.collider.CompareTag ("WoodenFloor") && !Input.GetKey (KeyCode.LeftShift) && Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_WoodStepsSneak, 0.4f);
			}
			else if (hit.collider.CompareTag ("StoneFloor") && Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_StoneStepsRun, 0.55f);
			}
			else if (hit.collider.CompareTag ("StoneFloor") && !Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_StoneStepsWalk, 0.7f);
			}
			else if (hit.collider.CompareTag ("StoneFloor") && !Input.GetKey (KeyCode.LeftShift) && Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_StoneStepsSneak, 0.75f);
			}
			else if (hit.collider.CompareTag ("Carpet") && Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_CarpetStepsRun, 0.4f);
			}
			else if (hit.collider.CompareTag ("Carpet") && !Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_CarpetStepsWalk, 0.45f);
			}
			else if (hit.collider.CompareTag ("Carpet") && !Input.GetKey (KeyCode.LeftShift) && Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_CarpetStepsSneak, 0.4f);
			}
			else if (hit.collider.CompareTag ("Grass") && Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_GrassStepsRun, 0.4f);
			}
			else if (hit.collider.CompareTag ("Grass") && !Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_GrassStepsWalk, 0.4f);
			}
			else if (hit.collider.CompareTag ("Grass") && !Input.GetKey (KeyCode.LeftShift) && Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_GrassStepsSneak, 0.4f);
			}
			else if (Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_CarpetStepsRun, 0.4f);
			}
			else if (!Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_CarpetStepsWalk, 0.45f);
			}
			else if (!Input.GetKey (KeyCode.LeftShift) && Input.GetKey (KeyCode.LeftControl)) {
				PlayFootStepAudio (m_CarpetStepsSneak, 0.4f);
			}

		}

		private void UpdateCameraPosition (float speed){
			Vector3 newCameraPosition;
			if (!m_UseHeadBob) {
				return;
			}
			if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded) {
				m_Camera.transform.localPosition =
                    m_HeadBob.DoHeadBob (m_CharacterController.velocity.magnitude +
					(speed * (m_IsWalking ? 1f : m_RunstepLenghten)));
				newCameraPosition = m_Camera.transform.localPosition;
				newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset ();
			} else {
				newCameraPosition = m_Camera.transform.localPosition;
				newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset ();
			}
			m_Camera.transform.localPosition = newCameraPosition;
		}

		private void GetInput (out float speed){
			// Read input
			float horizontal = CrossPlatformInputManager.GetAxis ("Horizontal");
			float vertical = CrossPlatformInputManager.GetAxis ("Vertical");

			bool waswalking = m_IsWalking;

#if !MOBILE_INPUT
			// On standalone builds, walk/run speed is modified by a key press.
			// keep track of whether or not the character is walking or running
			m_IsWalking = !Input.GetKey (KeyCode.LeftShift);
#endif
			// set the desired speed to be walking or running
			speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
			m_Input = new Vector2 (horizontal, vertical);

			// normalize input if it exceeds 1 in combined length:
			if (m_Input.sqrMagnitude > 1) {
				m_Input.Normalize ();
			}

			// handle speed change to give an fov kick
			// only if the player is going to a run, is running and the fovkick is to be used
			if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0) {
				StopAllCoroutines ();
				StartCoroutine (!m_IsWalking ? m_FovKick.FOVKickUp () : m_FovKick.FOVKickDown ());
			}
		}

		private void RotateView (){
			m_MouseLook.LookRotation (transform, m_Camera.transform);
		}

		private void OnControllerColliderHit (ControllerColliderHit hit){
			Rigidbody body = hit.collider.attachedRigidbody;
			//dont move the rigidbody if the character is on top of it
			if (m_CollisionFlags == CollisionFlags.Below) {
				return;
			}

			if (body == null || body.isKinematic) {
				return;
			}
			body.AddForceAtPosition (m_CharacterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
		}
	}
}
