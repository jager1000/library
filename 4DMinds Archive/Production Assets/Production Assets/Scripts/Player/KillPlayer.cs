﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections;

public class KillPlayer : MonoBehaviour {

    public GameObject librarian;
    public Transform libPos;
    public GameObject player;
    public GameObject fadeOutObject;
    public float speed;
    private bool fpsScript;
    private bool charC;
	private int waitASec = 0;

	private AudioSource source;
	public AudioClip scream;
	private bool playedSound = false;
	public float screamVolume = 0.85f;

	void Start () {
        //this.GetComponent<CharacterController>().enabled = false;
        librarian.SetActive(true);
        this.GetComponent<FirstPersonController>().enabled = false;

        fadeOutObject.SetActive(true);
		source = GetComponent<AudioSource> ();

	}
	
	// Update is called once per frame
        void Update() {
		transform.rotation = Quaternion.Lerp (transform.rotation, libPos.rotation, speed * Time.deltaTime);
		//drop camera
		waitASec ++;
		if (waitASec == 95) {
			this.GetComponent<CharacterController> ().enabled = false;
			this.GetComponent<ConstantForce> ().enabled = true;
			this.GetComponent<Rigidbody> ().isKinematic = false;
		}
		//end drop camera

		//this.transform.LookAt(libPos);
		if (!playedSound) {
			source.PlayOneShot (scream, screamVolume);
			playedSound = true;
		}
	}
}
